/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import sinon from 'sinon';
import chai from 'chai';
import Utils from '../../built/common/utils';

const { expect, assert } = chai;

/**
 * @author Alessandro Alberga
 * @description Simple intial test.
 */
describe('1 - Initial unit test', () => {
  describe('1.1 - Utils', () => {
    it('Should run PaginateResults successfully', () => {
      let queryObject: any = {
        id: 1
      };
      queryObject = Utils.PaginateResults(queryObject, 1, 100);
      expect(queryObject).to.deep.equal({
        id: 1,
        offset: 100,
        limit: 100
      });
    });

    it('Should run MapRecordToRowAndCount successfully', () => {
      const record = Utils.MapRecordToRowAndCount([{ id: 1 }], 72);
      expect(record).to.deep.equal({
        count: 72,
        rows: [{ id: 1 }]
      });
    });
  });
});
