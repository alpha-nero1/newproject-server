/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description cross platform configuration constants.
 */
export const Constants = {
  // Expiry increase of user auth token. In seconds.
  expiryIncrease: (60 * 60 * 24),
  // Window users have to reset their password. In seconds.
  authResetWindow: (60 * 60 * 24),
  // Seconds to throttle user auth reset creation.
  authResetThrottle: 30,
  reportThreshold: 5,
  initialCommentLimit: 10
};
