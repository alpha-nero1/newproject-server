/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description Exports a TS enum detailing common errors within the app.
 */
enum AppErrors {
  UnauthorisedError = 'You are not authorised for this action.',
  NotAuthenticated = 'You are not authenticated'
}

export default AppErrors;
