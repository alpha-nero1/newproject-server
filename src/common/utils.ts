/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import bcrypt from 'bcrypt';
import config from '../config/config.json';
import { Constants } from './constants';
import { GeneralQueryOpts } from '../schema/types/general-query-opts.interface';

/**
 * @author Alessandro Alberga
 * @description usefull utils class for backend!
 */
export default class Utils {

  public static MapRecordToRowAndCount(array: any[], count: number) {
    return {
      count,
      rows: array
    };
  }

  /**
   * Stringify query options.
   *
   * @param opts general query options.
   */
  public static StringifyQueryOptions(opts: GeneralQueryOpts): string {
    let str = '';
    if (opts.page_size) {
      if (opts.page) {
        str = `OFFSET ${opts.page_size * opts.page}`;
      }
      str += ` LIMIT ${opts.page_size}`;
    }
    return str;
  }

  /**
   * Pattern to split a simple array into an object with properties count and rows.
   * Limits the rows on certain use cases.
   *
   * @param array array to process.
   */
  public static ArrayToRowsAndCount(array: any[], rowLimit?: number): any {
    const count = array.length;
    let returnArray = [...array];
    if (rowLimit) {
      returnArray = returnArray.slice(0, rowLimit);
    }
    return {
      count,
      rows: returnArray
    };
  }

  /**
   * One stop shop for encrypting in the app.
   *
   * @param text text to encrypt.
   */
  public static encrypt(text: string): Promise<string> {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(config.jwt.salt_rounds, (err, salt) => {
        if (err) { reject(err); }
        bcrypt.hash(text, salt, (hashErr, hash) => {
          if (hashErr) {
            reject(hashErr);
          } else {
            resolve(hash);
          }
        });
      });
    });
  }

  /**
   * Get new auth token expiry date.
   */
  public static getNewAuthTokenExpiryDate(): Date {
    const expiryDate = new Date();
    const expirySeconds = new Date().getSeconds();
    expiryDate.setSeconds(expirySeconds + Constants.expiryIncrease);
    return expiryDate;
  }

  /**
   * Get new auth reset expiry date.
   */
  public static getAuthResetExpiryDate(): Date {
    const expiryDate = new Date();
    const expirySeconds = new Date().getSeconds();
    expiryDate.setSeconds(expirySeconds + Constants.authResetWindow);
    return expiryDate;
  }

  /**
   * Paginate results static function helper.
   *
   * @param query query to run.
   * @param page page value.
   * @param pageSize page size.
   */
  public static PaginateResults(query: any, page = 0, pageSize = 20): any {
    const offset = page * pageSize;
    const limit = pageSize;
    const returnQuery = query;
    returnQuery.offset = offset;
    returnQuery.limit = limit;
    return returnQuery;
  }

  /**
   * Elipsify text given a max length.
   *
   * @param text text to potentially elipsify.
   * @param maxLength max length that text is allowed to be.
   */
  public static ElipsifyText(text: string, maxLength: number) {
    if (maxLength > text.length) {
      return `${text.substr(0, maxLength)}...`;
    }
    return text;
  }
}
