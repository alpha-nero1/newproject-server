/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description a custom logger class to format how we log warnings, errors, e.t.c
 */
export class CustomLogger {

  /**
   * Log a message.
   *
   * @param message message to log to the console.
   * @param rest objects to log to the console.
   */
  public static Log(message: string, ...rest): void {
    console.log(`\n** INFO: ${message}`, ...rest);
  }

  /**
   * Log a warning.
   *
   * @param warning warning message.
   * @param rest objects to log to the console.
   */
  public static Warning(warning, ...rest): void {
    console.log(`\n** WARNING: ${warning}`, ...rest);
  }

  /**
   * Log an error.
   *
   * @param error error message.
   * @param rest objects to log to the console.
   */
  public static Error(error, ...rest): void {
    console.log(`\n** ERROR: ${error} \n ${error?.message} \n ${error?.stack}`, ...rest);
  }
}
