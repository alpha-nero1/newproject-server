/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import uuid from 'uuid/v4';
import { Constants } from '../common/constants';

export default (sequelize: any, DataTypes: any) => {
  const expiryDate = new Date(new Date().setDate(new Date().getDay() + Constants.authResetWindow));

  const UserAuthReset = sequelize.define('user_auth_resets', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    user_auth_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users_auths',
        key: 'id'
      }
    },
    reset_token: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: uuid()
    },
    expiry_date: {
      allowNull: false,
      defaultValue: expiryDate,
      type: DataTypes.DATE
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    disabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, { timestamps: false });

  UserAuthReset.associate = (models) => {
    UserAuthReset.belongsTo(models.user_auths, {
      foreignKey: 'user_auth_id'
    });
  };

  return UserAuthReset;
};
