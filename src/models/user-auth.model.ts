/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

export default (sequelize: any, DataTypes: any) => {
  const UserAuth = sequelize.define('user_auths', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    handle: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    disabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, { timestamps: false });

  UserAuth.associate = (models) => {
    UserAuth.belongsTo(models.users, {
      foreignKey: 'user_id'
    });
    UserAuth.hasMany(models.user_auth_tokens, {
      foreignKey: 'user_auth_id'
    });
    UserAuth.hasMany(models.user_auth_resets, {
      foreignKey: 'user_auth_id'
    });
  };

  return UserAuth;
};
