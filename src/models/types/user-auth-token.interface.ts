/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @descrizione user auth token interface description.
 */
export interface UserAuthToken {
  /**
   * Id of user auth token.
   */
  id?: number;

  /**
   * Token used to authenticate user.
   */
  token: string;

  /**
   * Exiration date keeping token volatile.
   */
  expiry_date: Date;

  /**
   * Created at date.
   */
  created_at?: Date;

  /**
   * Updated at date.
   */
  updated_at?: Date;

  /**
   * Diabled toggle.
   */
  disabled?: boolean;
}
