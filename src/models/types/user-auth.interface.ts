/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description user auth interface definition.
 */
export interface UserAuth {
  /**
   * User auth id.
   */
  id?: number;

  /**
   * Handle of user auth login.
   */
  handle: string;

  /**
   * Connected user user id.
   */
  user_id?: number;

  /**
   * Encrypted password for user auth.
   */
  password?: string;

  /**
   * Created at date.
   */
  created_at?: Date;

  /**
   * Updated at date.
   */
  updated_at?: Date;

  /**
   * Disabled.
   */
  disabled?: boolean;
}
