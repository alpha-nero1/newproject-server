/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';

/**
 * @author Alessandro Alberga
 * @description user auth interface definition.
 */
export interface User extends StandardDatabaseRecord {
  /**
   * Id of user.
   */
  id: number;

  /**
   * First name of user.
   */
  first_name?: string;

  /**
   * Last name of user.
   */
  last_name?: string;

  /**
   * User nickname.
   */
  nickname?: string;

  /**
   * Usernam of user.
   */
  username?: string;

  /**
   * Email of user.
   */
  email?: string;

  /**
   * Date of birth of user.
   */
  date_of_birth?: Date;

  /**
   * Personalisation tags of user.
   */
  tags: string[];

  /**
   * Profile photo uri.
   */
  profile_photo: string;

  /**
   * Banner photo of user.
   */
  banner_photo?: string;

  /**
   * Pemission role bits of user.
   */
  role_bits: number;
}
