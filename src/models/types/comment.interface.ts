/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';
import { User } from './user.interface';
import { Site } from './site.interface';

/**
 * @author Alessandro Alberga
 * @description Site comment interface.
 */
export interface Comment extends StandardDatabaseRecord {
  /**
   * Sequelize datavalues.
   */
  dataValues?: any;

  /**
   * Comment text.
   */
  text: string;

  /**
   * User id.
   */
  user_id: number;

  /**
   * Id of parent that was commented on.
   */
  parent_comment_id: number;

  /**
   * Replies of comment.
   */
  replies?: any;

  /**
   * Included user model.
   */
  user?: User;

  /**
   * Included note parent model.
   */
  parent?: Comment;

  /**
   * Potentially included site model.
   * (Via link table, site_comments)
   */
  site?: Site;

  /**
   * Number of counts on comment.
   */
  vote_count?: number;
}
