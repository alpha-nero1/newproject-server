/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description email template interface.
 */
export interface EmailTemplate {

  /**
   * Unique email template name.
   */
  template_name: string;

  /**
   * Data to inject into email template.
   */
  template_data: any;
}
