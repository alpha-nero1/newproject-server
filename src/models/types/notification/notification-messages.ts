/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { User } from '../user.interface';
import Utils from '../../../common/utils';

const GetMessagePrefix = (user: User) => `${user.first_name} ${user.last_name} (${user.username})`;

/**
 * @author Alessandro Alberga
 * @description object containing string functions.
 */
export const NotificationMessages = {
  SomeoneCommentedOnYourSite: ({ user, site, comment }) => (`
    ${GetMessagePrefix(user)} commented ${Utils.ElipsifyText(comment?.text, 20)} on your site **${site?.title}**
  `),
  SomeoneRepliedToYourComment: ({ user, comment, reply }) => (`
    ${GetMessagePrefix(user)} replied ${Utils.ElipsifyText(reply?.text, 20)} to your comment **${comment?.text}**
  `)
};
