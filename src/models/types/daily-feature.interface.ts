/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';

/**
 * @author Alessandro Alberga
 */
export interface DailyFeature extends StandardDatabaseRecord {
  /**
   * Id of feature.
   */
  id?: number;
  /**
   * Title of the feature.
   */
  title?: string;
  /**
   * Button text.
   */
  button_text?: string;
  /**
   * Id of the site.
   */
  site_id?: number;
}
