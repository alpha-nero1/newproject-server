/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';
import { SiteReport } from './site-report.interface';

/**
 * @author Alessandro Alberga
 */
export interface Site extends StandardDatabaseRecord {
  /**
   * Id of site.
   */
  id?: number;

  /**
   * Actual content of site.
   */
  content?: string;

  /**
   * Title of site.
   */
  title?: string;

  /**
   * Uuid of site.
   */
  uuid?: string;

  /**
   * Tags associated with site.
   */
  tags?: string[];

  /**
   * Set to true if threshold of reports are met.
   */
  passed_report_threshold?: boolean;

  /**
   * Potentially included site reports model.
   */
  reports: SiteReport[];

  /**
   * Id of user.
   */
  user_id?: number;

  /**
   * Banner photo of site.
   */
  banner_photo?: string;

  /**
   * Sequelize add comment handler.
   */
  addComment: (comment, transactrion) => any;
}
