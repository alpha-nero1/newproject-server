/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';

/**
 * @author Alessandro Alberga
 * @description Role interface descriptor.
 */
export interface Role extends StandardDatabaseRecord {
  /**
   * Id of the role.
   */
  id?: number;

  /**
   * Role name.
   */
  role?: string;

  /**
   * Role bits associated with role.
   */
  role_bits: number;
}
