/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description User statistics of a user.
 */
export interface UserStats {

  /**
   * How many sites created by user.
   */
  total_sites_created?: number;

  /**
   * Total comments user has made.
   */
  total_comments_made?: number;

  /**
   * Total site upvotes user has gotten.
   */
  total_site_upvotes?: number;

  /**
   * Total site downvotes user has gotten.
   */
  total_site_downvotes?: number;

  /**
   * Total subscribers user has.
   */
  total_subscribers?: number;

  /**
   * Total of users that user is subscribed to.
   */
  total_subscribed_to?: number;
}
