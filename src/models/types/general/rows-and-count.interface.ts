/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description Rows and count type, descries what we get back from findAndCountAll
 */
export interface RowsAndCount<T> {
  /**
   * Count of all results.
   */
  count: number;
  /**
   * Rows, paged subset of results.
   */
  rows: T[];
}
