/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';

/**
 * @author Alessandro Alberga
 * @description Vote interface.
 */
export interface Vote extends StandardDatabaseRecord {
  /**
   * If is site vote.
   */
  site_id?: number;

  /**
   * User id who voted.
   */
  user_id: number;

  /**
   * Comment id.
   */
  comment_id?: number;
}
