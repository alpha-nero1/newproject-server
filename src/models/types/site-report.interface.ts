/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { StandardDatabaseRecord } from './standard-database-record.interface';
import { User } from './user.interface';

/**
 * @author Alessandro Alberga
 */
export interface SiteReport extends StandardDatabaseRecord {
  /**
   * Id of site report.
   */
  id?: number;
  /**
   * User who reported site.
   */
  reporting_user_id: number;
  /**
   * The site reported.
   */
  site_id: number;
  /**
   * Included site model.
   */
  site?: any;
  /**
   * Reporting user.
   */
  reporting_user: User;
  /**
   * Indicator that report is dismissed.
   */
  dismissed?: boolean;
}
