/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file specifying a site model.
 */
export default (sequelize: any, DataTypes: any) => {
  const SiteReport = sequelize.define(
    'site_reports',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      site_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'sites',
          key: 'id'
        }
      },
      reason: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      reporting_user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      dismissed: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      disabled: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      created_at: {
        allowNull: false,
        defaultValue: DataTypes.NOW,
        type: DataTypes.DATE
      },
      updated_at: {
        allowNull: false,
        defaultValue: DataTypes.NOW,
        type: DataTypes.DATE
      }
    },
    { timestamps: false }
  );
  SiteReport.associate = (models: any) => {
    SiteReport.belongsTo(models.users, {
      as: 'reporting_user',
      foreignKey: 'reporting_user_id'
    });
    SiteReport.belongsTo(models.sites, {
      as: 'site',
      foreignKey: 'site_id'
    });
  };
  return SiteReport;
};
