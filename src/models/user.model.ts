/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description User model definition.
 */
export default (sequelize: any, DataTypes: any) => {
  const User = sequelize.define(
    'users',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      first_name: {
        type: DataTypes.STRING
      },
      last_name: {
        type: DataTypes.STRING
      },
      nickname: {
        type: DataTypes.STRING
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING
      },
      date_of_birth: {
        type: DataTypes.DATE
      },
      tags: {
        type: DataTypes.ARRAY(DataTypes.STRING)
      },
      role_bits: {
        type: DataTypes.INTEGER
      },
      auth_token: {
        type: DataTypes.TEXT
      },
      profile_photo: {
        type: DataTypes.STRING
      },
      banner_photo: {
        type: DataTypes.STRING
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      disabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      }
    },
    { timestamps: false }
  );
  User.associate = (models: any) => {
    User.hasMany(models.user_auths, {
      foreignKey: 'user_id'
    });
    User.hasMany(models.sites, {
      foreignKey: 'user_id',
      as: 'sites'
    });
    User.hasMany(models.subscriptions, {
      as: 'subscribers',
      foreignKey: 'subscribee_user_id'
    });
    User.hasMany(models.subscriptions, {
      as: 'subscriptions',
      foreignKey: 'subscriber_user_id'
    });
  };
  return User;
};
