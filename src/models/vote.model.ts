/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description Vote model definition.
 */
export default (sequelize: any, DataTypes: any) => {
  const Votes = sequelize.define('votes', {
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    value: {
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    disabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
  Votes.associate = () => { };
  return Votes;
};
