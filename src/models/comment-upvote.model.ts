/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description Site upvote model definition.
 */
export default (sequelize: any, DataTypes: any) => {
  const CommentDownvotes = sequelize.define('comment_upvotes', {
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    comment_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    disabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
    { timestamps: false });
  CommentDownvotes.associate = (models: any) => {
    CommentDownvotes.belongsTo(models.users, { foreignKey: 'user_id' });
    CommentDownvotes.belongsTo(models.comments, { foreignKey: 'comment_id' });
  };
  return CommentDownvotes;
};
