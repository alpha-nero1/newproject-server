/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file specifying a site model.
 */
export default (sequelize: any, DataTypes: any) => {
  const DailyFeature = sequelize.define(
    'daily_features',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING
      },
      button_text: {
        type: DataTypes.STRING
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      disabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      site_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
    },
    {
      timestamps: false
    }
  );
  DailyFeature.associate = (models: any) => {
    DailyFeature.belongsTo(models.sites, {
      as: 'site',
      foreignKey: 'site_id',
      targetKey: 'id'
    });
  };
  return DailyFeature;
};
