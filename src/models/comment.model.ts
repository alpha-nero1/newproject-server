/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file specifying a comment model.
 */
export default (sequelize: any, DataTypes: any) => {
  const Comment = sequelize.define('comments', {
    id: {
      allowNull: false,
      primaryKey: true,
      unique: true,
      autoIncrement: true,
      type: DataTypes.INTEGER
    },
    text: {
      type: DataTypes.TEXT
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    parent_comment_id: {
      type: DataTypes.INTEGER
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    disabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
    { timestamps: false });

  Comment.associate = (models: any) => {
    Comment.belongsToMany(models.sites, {
      through: 'site_comments',
      foreignKey: 'comment_id'
    });
    Comment.hasMany(models.site_comments, {
      as: 'dummy_middleman_association',
      foreignKey: 'comment_id'
    });
    Comment.belongsTo(models.users, {
      as: 'user',
      foreignKey: 'user_id'
    });
    Comment.hasMany(models.comments, {
      as: 'replies',
      foreignKey: 'parent_comment_id'
    });
    Comment.hasMany(models.comment_upvotes, {
      as: 'upvotes',
      foreignKey: 'comment_id'
    });
    Comment.hasMany(models.comment_downvotes, {
      as: 'downvotes',
      foreignKey: 'comment_id'
    });
  };
  return Comment;
};
