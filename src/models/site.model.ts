/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

const uuid = require('uuid/v4');

/**
 * @author Alessandro Alberga
 * @description file specifying a site model.
 */
export default (sequelize: any, DataTypes: any) => {
  const Site = sequelize.define(
    'sites',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      uuid: {
        allowNull: false,
        type: DataTypes.UUID,
        defaultValue: DataTypes.NOW
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING
      },
      banner_photo: {
        type: DataTypes.STRING
      },
      content: {
        type: DataTypes.TEXT
      },
      tags: {
        type: DataTypes.ARRAY(DataTypes.STRING)
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      disabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        }
      }
    },
    {
      timestamps: false,
      setterMethods: {
        // Critical to make sure we set an entirely new uuid on each creation.
        uuid() {
          this.setDataValue('uuid', uuid());
        }
      }
    }
  );
  Site.associate = (models: any) => {
    Site.belongsTo(models.users, {
      as: 'user',
      foreignKey: 'user_id',
      targetKey: 'id'
    });
    Site.belongsToMany(models.comments, {
      as: 'comments',
      through: 'site_comments',
      foreignKey: 'site_id'
    });
    Site.hasMany(models.site_upvotes, {
      as: 'upvotes',
      foreignKey: 'site_id'
    });
    Site.hasMany(models.site_downvotes, {
      as: 'downvotes',
      foreignKey: 'site_id'
    });
    Site.hasMany(models.site_reports, {
      as: 'reports',
      foreignKey: 'site_id'
    });
  };
  return Site;
};
