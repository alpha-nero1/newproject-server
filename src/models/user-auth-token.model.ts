/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Sequelize, DataTypes } from 'sequelize';

export default (sequelize: Sequelize) => {
  const UserAuthToken: any = sequelize.define('user_auth_tokens', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    user_auth_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    expiry_date: {
      allowNull: false,
      defaultValue: new Date(new Date().setDate(new Date().getDay() + 7)),
      type: DataTypes.DATE
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    disabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, { timestamps: false });

  UserAuthToken.associate = (models) => {
    UserAuthToken.belongsTo(models.user_auths, {
      foreignKey: 'user_auth_id'
    });
  };

  return UserAuthToken;
};
