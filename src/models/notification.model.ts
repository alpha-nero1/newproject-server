/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file specifying a notification model.
 */
export default (sequelize: any, DataTypes: any) => {
  const Notification = sequelize.define(
    'notifications',
    {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        autoIncrement: true,
        type: DataTypes.INTEGER
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING
      },
      slug: {
        type: DataTypes.STRING
      },
      icon: {
        type: DataTypes.STRING
      },
      read_time: {
        type: DataTypes.DATE
      },
      created_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW
      },
      disabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        }
      }
    },
    {
      timestamps: false
    }
  );
  Notification.associate = (models: any) => {
    Notification.belongsTo(models.users, {
      as: 'user',
      foreignKey: 'user_id',
      targetKey: 'id'
    });
  };
  return Notification;
};
