/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file containing gql definitions of application queries.
 */
export default `
  extend type Query {
    # List users according to parameters
    listUsers(
      username: String
      first_name: String
      last_name: String
      email: String
      ): [User]

    # Find a full user.
    findFullUser(username: String): User

    # List recommended sites according to user.
    listRecommendedSitesByUser(user_id: Int, query_opts: GeneralQueryOpts): SitesWithCount

    # List trending sites of the website.
    listTrendingSites(query_opts: GeneralQueryOpts): SitesWithCount

    # Get a site by its uuid.
    getSiteByUuid(uuid: String): Site

    # Find all comments that are on a site.
    findCommentsBySite(site_id: Int!, query_opts: GeneralQueryOpts): CommentsWithCount

    # Fetch comments by their ids.
    findCommentsByIds(ids: [Int], query_opts: GeneralQueryOpts): [Comment]

    # Find all comments that are on a site.
    findRepliesByComment(comment_id: Int!, query_opts: GeneralQueryOpts): CommentsWithCount

    # Get users who a user has subscribed to.
    getMySubscribees(user_id: Int): SubscriptionResponse

    # Get users who have subscribed to a user.
    getMySubscribers(user_id: Int): SubscriptionResponse

    # Search sites in the db.
    searchSites(keyword: String, tags: [String], query_opts: GeneralQueryOpts): SitesWithCount

    # Get credentials to be able to upload.
    getTemporaryCredentials: AwsCredentials

    # Get a users sites with votes.
    getSitesWithVotes(up: Boolean, page: Int): SitesWithCount

    # Get all reported sites according to params sent in.
    getReportedSites(opts: ListReportedSitesParams): SitesWithCount

    # Get all site report objects on a site.
    getSiteReports(opts: ListReportedSitesParams): SiteReportsWithCount

    # Get all roles.
    getAllRoles: [Role]

    getDailyFeatures(opts: GeneralQueryOpts!): DailyFeaturesWithCount

    # Get a users activities, is context based.
    getNotifications(opts: GeneralQueryOpts!): NotificationsWithCount

    getUserStats(username: String!): UserStats

    getUsersSites(username: String!, query_opts: GeneralQueryOpts): SitesWithCount
  }
`;
