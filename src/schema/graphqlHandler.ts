/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import AppErrors from '../common/appErrors';
import { CustomLogger } from '../common/customLogger';

/**
 * Takes a graphql api as a param and returns a function to handle
 * graphql requests.
 *
 * @param api graphql api.
 */
export const GraphqlHandler = api => (req, res, next) => ({
  schema: api,
  context: { headers: req.headers },
  customFormatErrorFn: (err) => {
    CustomLogger.Error(err);
    // If not authed send 401
    if (err.message && err.message === AppErrors.NotAuthenticated) {
      res.statusCode = 401;
      return '**WARNING: You are not authenticated.';
    } else {
      // Else, the program mucked up, send back a 500.
      res.statusCode = 500;
      return '**FATAL: Internal Server Error.';
    }
  }
});
