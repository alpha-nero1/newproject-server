/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file containing gql definition of return types.
 */
export default `
  # Date return scalar type.
  scalar Date

  type Query {
    me: User
  }

  # Role return type.
  type Role {
    id: Int
    role: String
    role_bits: Int
  }

  type SiteReport {
    id: Int
    reporting_user_id: Int
    reporting_user: User
    site_id: Int
    reason: String
    site: Site
    dismissed: Boolean
  }

  type SitesWithCount {
    count: Int
    rows: [Site]
  }

  type SiteReportsWithCount {
    count: Int
    rows: [SiteReport]
  }

  # Standard response for success/failure operations.
  type StandardResponse {
    success: String
    reason: String
  }

  input ListReportedSitesParams {
    site_id: Int
    after_date: Date
    before_date: Date
    dismissed: Boolean
    site_passed_threshold: Boolean
    disabled: Boolean
    sort_by_prop: String
    sort_by_value: String
    page: Int
    page_size: Int
    keyword: String
  }

  input GeneralQueryOpts {
    id: Int
    sort_by_property: String
    sort_by_value: String
    page: Int
    page_size: Int
    keyword: String
    disabled: Boolean
  }

  # Type denoting a site object.
  type User {
    id: Int
    email: String
    username: String
    first_name: String
    last_name: String
    date_of_birth: Date
    tags: [String]
    sites: [Site]
    auth_token: String
    subscriptions: SubscriptionResponse
    subscribers: SubscriptionResponse
    profile_photo: String
    role_bits: Int
    banner_photo: String
  }

  # Type denoting a site object.
  type Site {
    id: Int
    title: String
    content: String
    user_id: Int
    uuid: String
    tags: [String]
    user: User
    created_at: Date
    updated_at: Date
    comments: CommentsWithCount
    upvotes: VotesWithCount
    downvotes: VotesWithCount
    reports: [SiteReport]
    site_passed_threshold: Boolean
    banner_photo: String
    private: Boolean
  }

  type DailyFeature {
    id: Int
    title: String
    button_text: String
    site_id: Int
    site: Site
    created_at: Date
    disabled: Boolean
    updated_at: Date
  }

  # Input for user creation.
  input UserInput {
    id: Int
    first_name: String!
    last_name: String!
    username: String!
    email: String!
    password: String!
    confirm_password: String!
    tags: [String]
    date_of_birth: Date
    profile_photo: String
  }

  # A relaxed version of the UserInput input to allow
  # for versatile user property updating.
  input UserUpdate {
    id: Int
    first_name: String
    last_name: String
    username: String
    email: String
    password: String
    confirm_password: String
    tags: [String]
    date_of_birth: Date
    profile_photo: String
    banner_photo: String
  }

  # Authentication input.
  input AuthInput {
    email: String!
    password: String!
  }

  # Input object for a site.
  input SiteInput {
    title: String!
    content: String!
    tags: [String]
    banner_photo: String
    private: Boolean
  }

  # Input for personal details
  input PersonalDetailsInput {
    id: ID!
    email: String!
    username: String!
    first_name: String
    last_name: String
  }

  # Comment output type.
  type Comment {
    id: Int
    parent_comment_id: Int
    text: String
    user_id: Int
    created_at: Date
    user: User
    replies: CommentsWithCount
    upvotes: VotesWithCount
    downvotes: VotesWithCount
    vote_count: Int
  }

  # Input for creating a comment
  input CommentInput {
    text: String,
    parent_comment_id: Int
  }

  # Structure for sending votes back to ui.
  type VotesWithCount {
    count: Int
    rows: [Vote]
  }

  # What is contained in the lists for VoteResponse
  type Vote {
    id: Int
    user: User
  }

  # Subscription record return type.
  # ATTENTION: Subscription is a reserved word and it CANNOT
  # be reliably used as a return type.
  type Sub {
    id: ID
    user_id: Int
    subscribee_id: Int
    created_at: Date
    subscriber: User
    subscribee: User
  }

  # Input data required to create a subscription.
  input SubscriptionInput {
    user_id: Int
    # User who is being subscribed to.
    subscribee_id: Int
  }

  # Subscription response including count of subs and actual subs.
  type SubscriptionResponse {
    count: Int
    rows: [Sub]
  }

  # Credentials to access s3 temporarily.
  type AwsCredentials {
    SessionToken: String
    AccessKeyId: String
    SecretAccessKey: String
  }

  # User auth response for authentication.
  type UserAuthResponse {
    token: String
    expiry_date: Date
  }

  type DailyFeaturesWithCount {
    count: Int
    rows: [DailyFeature]
  }

  input DailyFeatureInput {
    site_id: Int!
    title: String!
    button_text: String!
  }

  type Notification {
    id: Int
    user_id: Int
    slug: String
    title: String
    icon: String
    created_at: Date
    read_time: Date
  }

  type NotificationsWithCount {
    count: Int
    rows: [Notification]
  }

  type CommentsWithCount {
    count: Int
    rows: [Comment]
  }

  type UserStats {
    total_sites_created: Int
    total_comments_made: Int
    # Totals that this person has accumulated NOT done.
    total_site_upvotes: Int
    total_site_downvotes: Int
    total_subscribers: Int
    total_subscribed_to: Int
  }
`;
