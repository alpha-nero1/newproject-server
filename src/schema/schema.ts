/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2019: The New Project.
 * All Rights Reserved.
 */

import applicationQueries from './applicationQueries';
import applicationTypes from './applicationTypes';
import applicationMutations from './applicationMutations';
import { GraphQLScalarType, Kind } from 'graphql';
import { RegisteredServices } from './types/registered-services.interface';
import { AdminGuard } from '../database/guards/admin.guard';
import { ActingUserGuard } from '../database/guards/acting-user.guard';
import { AppRole } from '../database/guards/app-role';

/**
 * @author Alessandro Alberga
 * @description accepts services (called from dependency injected container)
 * and returns the resolvers necessary to run the api.
 */

// Takes gql schema strings defined elsewhere.
const schema = [applicationQueries, applicationTypes, applicationMutations];

// Takes services as inputs and returns gql resolvers.
const resolvers = (services: RegisteredServices) => ({
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Custom date scalar for gql',
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return value; // value sent to the client
    },
    parseLiteral(ast: any) {
      if (ast.kind === Kind.INT) {
        // ast value is always in string format.
        return new Date(ast.value);
      }
      return ast;
    }
  }),
  Query: {
    // Get MY details.
    me(root, args, { user }) {
      return services.UserService.me(user);
    },
    getSitesWithVotes(root, args, { user }) {
      return services.SiteService.getSitesWithVotes(user, args.up, args.page);
    },
    // List all users according to arguments.
    listUsers(root, args, context) {
      return services.UserService.findUsers(args.first_name, args.last_name, args.email);
    },
    // Find a full user, including models.
    findFullUser(root, args, context) {
      return services.UserService.findFullUser(args.username, context);
    },
    // List a user's recomended sites.
    listRecommendedSitesByUser(root, args, { user }) {
      return services.SiteService.listRecommendedSitesByUser(user, args.query_opts);
    },
    // List all the trending sites.
    listTrendingSites(root, args, context) {
      return services.SiteService.listTrendingSites(args.query_opts);
    },
    // Get site by uuid.
    getSiteByUuid(root, args, context) {
      return services.SiteService.getSiteByUuid(args.uuid, context);
    },
    findCommentsBySite(root, args, { user }) {
      return services.CommentService.findCommentsBySiteId(args.site_id, user.id, args.query_opts);
    },
    findCommentsByIds(root, args, { user }) {
      return services.CommentService.findCommentsByIds(args.ids, user, args.query_opts);
    },
    findRepliesByComment(root, args, { user }) {
      return services.CommentService.findRepliesByCommentId(args.comment_id, user, args.query_opts);
    },
    // Get people I have subscribed to.
    getMySubscribees(root, args, context) {
      return services.SubscriptionService.getMySubscribees(context.user.id);
    },
    // Get people who are subscribed to me.
    getMySubscribers(root, args, context) {
      return services.SubscriptionService.getMySubscribers(context.user.id);
      // Search sites using parameters.
    },
    searchSites(root, args) {
      return services.SiteService.searchSites(args.keyword, args.tags, args.query_opts);
    },
    getTemporaryCredentials(root, args, context) {
      if (!context.user) {
        return Promise.resolve(false);
      }
      return services.AwsManager.getTemporaryCredentials();
    },
    getAllRoles(root, args, { user }) {
      return AdminGuard(user, AppRole.Admin, () => services.RoleService.getRoles());
    },
    getSiteReports(root, args, { user }) {
      return AdminGuard(user, AppRole.Admin, () => services.SiteReportService.findAllByOptions(args.opts));
    },
    getReportedSites(root, args, { user }) {
      return AdminGuard(user, AppRole.Admin, () => services.SiteReportService.getReportedSitesByParams(args.opts));
    },
    getDailyFeatures(root, args, { user }) {
      return services.SiteService.getDailyFeatures(args.opts);
    },
    getNotifications(root, args, { user }) {
      return services.NotificationService.getNotifications(args.opts, user);
    },
    getUserStats(root, args) {
      return services.UserService.getUserStats(args.username);
    },
    getUsersSites(root, args) {
      return services.SiteService.getUsersSites(args.username, args.query_opts);
    }
  },
  Mutation: {
    register(root, args, context) {
      return services.AuthService.register(args.user);
    },
    updateUser(root, args, { user }) {
      return services.UserService.updateUser(user, args.new_user_props);
    },
    authenticateUser(root, args) {
      return services.AuthService.authenticate(args.handle, args.password);
    },
    createSite(root, args, context) {
      return services.SiteService.createSite(context, args.site);
    },
    updateSite(root, args, { user }) {
      return services.SiteService.getByPk(args.id)
      .then(record => (
        ActingUserGuard(user, record, () => services.SiteService.updateSite(args.id, args.new_values, user))
      ));
    },
    commentOnSite(root, args, { user }) {
      return services.CommentService.commentOnSite(args.site_id, args.comment, user);
    },
    replyToComment(root, args, { user }) {
      return services.CommentService.replyToComment(args.parent_comment_id, args.comment, user);
    },
    deleteComment(root, args, { user }) {
      return services.CommentService.findByPk(args.comment_id)
      .then((record) => (
        ActingUserGuard(user, record?.dataValues, () => services.CommentService.deleteComment(args.comment_id))
      ));
    },
    // Vote on a site with the user from root.
    voteOnSite(root, args, { user }) {
      return services.VoteService.voteOnSite(user, args.site_id, args.up);
    },
    // Vote on comment with the user from root.
    voteOnComment(root, args, { user }) {
      return services.VoteService.voteOnComment(user, args.comment_id, args.up);
    },
    // Subscribe to a user.
    subscribe(root, args, { user }) {
      return services.SubscriptionService.subscribe(user.id, args.subscribee_id);
    },
    // Unsubscribe to user.
    unsubscribe(root, args, { user }) {
      return services.SubscriptionService.unsubscribe(user.id, args.subscribee_id);
    },
    initiatePasswordRequest(root, args, { user }) {
      return services.AuthService.initiatePasswordReset(args.handle, user);
    },
    finalisePasswordRequest(root, args) {
      return services.AuthService.finalisePasswordReset(args.resetToken, args.newPassword);
    },
    reportSite(root, args, { user }) {
      return services.SiteReportService.reportSite(args.site_id, args.reason, user);
    },
    dismissSiteReports(root, args, { user }) {
      return AdminGuard(user, AppRole.Admin, () => services.SiteReportService.dismissSiteReports(args.site_report_ids));
    },
    assignRoleToUser(root, args, { user }) {
      return services.RoleService.assignRolesToUser(
        args.user_id,
        args.role_bits,
        user
      );
    },
    deleteUser(root, args, { user }) {
      return services.UserService.deleteUser(args.user_id, user);
    },
    restoreUser(root, args, { user }) {
      return services.UserService.restoreUser(args.user_id, user);
    },
    createDailyFeature(root, args, { user }) {
      return AdminGuard(user, AppRole.SuperAdmin, () => services.SiteService.createDailyFeature(args.daily_feature));
    }
  }
});

// Export resolvers and schema.
export {
  schema,
  resolvers
};
