/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import {
  makeExecutableSchema,
  addSchemaLevelResolveFunction
} from 'graphql-tools';
import { AuthService } from 'src/database/services/auth.service';

/**
 * @author Alessandro Alberga
 * @description file takes schema and resolvers returning the
 * executable schema for apollo.
 */
export default (schemas, resolvers, auth: AuthService) => {
  const schema = makeExecutableSchema({ typeDefs: [...schemas], resolvers });

  addSchemaLevelResolveFunction(schema, async (root, args, context, { operation }) => {
    // This is a middleware function so just add the user property to the context
    // and let it carry on its business.
    const authToken = context.headers.authorization || '';

    context.user = await auth.getUserFromUserAuthToken(authToken);
    if (context.user) {
      // Update auth token yah.
      auth.updateTokenExpiry(authToken);
    }
    // We need to handle kicking out an unauthed user.
  });
  return schema;
};
