/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { GeneralQueryOpts } from './general-query-opts.interface';

export interface ListReportedSitesParams extends GeneralQueryOpts {
  /**
   * Id of a site.
   */
  site_id?: number;
  /**
   * Date reported after.
   */
  after_date?: Date;
  /**
   * Date reported before.
   */
  before_date?: Date;
  /**
   * If the report is dismissed or not.
   */
  dismissed?: boolean;
  /**
   * Query only sites that have passed the threshold.
   */
  site_passed_threshold?: boolean;
  /**
   * Has the report been disabled.
   */
  disabled?: boolean;
}
