/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { UserService } from '../../database/services/user.service';
import { SiteService } from '../../database/services/site.service';
import { SiteReportService } from '../../database/services/site-report.service';
import { RoleService } from '../../database/services/role.service';
import { CommentService } from '../../database/services/comment.service';
import { SubscriptionService } from '../../database/services/subscription.service';
import { VoteService } from '../../database/services/vote.service';
import { AuthService } from '../../database/services/auth.service';
import { AwsManager } from '../../database/third-party/aws-manager.service';
import { NotificationService } from '../../database/services/notification.service';

/**
 * @author Alessandro Alberga
 */
export interface RegisteredServices {
  AuthService: AuthService;
  AwsManager: AwsManager;
  CommentService: CommentService;
  RoleService: RoleService;
  SiteReportService: SiteReportService;
  SiteService: SiteService;
  SubscriptionService: SubscriptionService;
  UserService: UserService;
  VoteService: VoteService;
  NotificationService: NotificationService;
}
