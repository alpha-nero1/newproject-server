/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description general query params for configuring a model query.
 */
export interface GeneralQueryOpts {
  /**
   * An id is a very common query condition.
   */
  id?: number;
  /**
   * Sort by property, e.g "first_name".
   */
  sort_by_property?: string;
  /**
   * Sort by value, e.g. "ASC"
   */
  sort_by_value?: string;
  /**
   * Page of results.
   */
  page?: number;
  /**
   * Page size of results.
   */
  page_size?: number;
  /**
   * Search keyword.
   */
  keyword?: string;
}
