/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description file containing gql definition for application mutations.
 */
export default `
  type Mutation {
    # Create a user on input.
    register(user: UserInput!): StandardResponse

    # Update a users properties.
    updateUser(new_user_props: UserUpdate!): User

    # Login and authenticate user assigning a token.
    authenticateUser(handle: String, password: String): UserAuthResponse

    # Create a new site.
    createSite(site: SiteInput!): Site

    # Update/edit a site.
    updateSite(id: Int!, new_values: SiteInput!): Site

    # Vote on a site.
    addSiteVote(user_id: Int, vote_value: Int): Boolean

    # Comment on a site. Returns comment created.
    commentOnSite(site_id: Int!, comment: CommentInput!): Comment

    replyToComment(parent_comment_id: Int!, comment: CommentInput!): Comment

    # Delete a comment return true if successfull.
    deleteComment(comment_id: Int): Comment

    # Vote on a site with the user from root.
    voteOnSite(site_id: Int, up: Boolean): Site

    # Vote on comment with the user from root.
    voteOnComment(comment_id: Int, up: Boolean): Comment

    # Subscribe to a users content using a subscription input object.
    subscribe(subscribee_id: Int): Sub

    # Unsubscribe to a users content using the subscribee_id
    unsubscribe(subscribee_id: Int): [Sub]

    # Initiate password reset request process.
    initiatePasswordRequest(handle: String): String

    # Finalise password request.
    finalisePasswordRequest(resetToken: String, newPassword: String): String

    # Report a site.
    reportSite(site_id: Int!, reason: String): SiteReport

    # Dismiss site reports.
    dismissSiteReports(site_report_ids: [Int]!): [SiteReport]

    # Allow assignin of roles (only to Emperor).
    assignRoleToUser(user_id: Int!, role_bits: Int): User

    # Delete a user.
    deleteUser(user_id: Int!): User

    # Restore a user.
    restoreUser(user_id: Int!): User

    # Create a new daily feature.
    createDailyFeature(daily_feature: DailyFeatureInput!): DailyFeature
  }
`;
