/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description creates a container for the service, persistence
 * schema and resolvers. Everything is appropriately instantiated in this file.
 */
import Bottle from 'bottlejs';

// Models.
import models from '../../models';

// Services.
import { AuthService } from '../services/auth.service';
import { AwsManager } from '../third-party/aws-manager.service';
import { CommentService } from '../services/comment.service';
import { SesManager } from '../third-party/ses-manager.service';
import { SiteService } from '../services/site.service';
import { SubscriptionService } from '../services/subscription.service';
import { UserService } from '../services/user.service';
import { VoteService } from '../services/vote.service';

// Persistence.
import { CommentPersistence } from '../persistence/comment.persistence';
import { SitePersistence } from '../persistence/site.persistence';
import { SubscriptionPersistence } from '../persistence/subscription.persistence';
import { UserPersistence } from '../persistence/user.persistence';
import { VotePersistence } from '../persistence/vote.persistence';

// Schema setup.
import api from '../../schema';

// Get schema and resolvers.
import { schema, resolvers } from '../../schema/schema';
import { UserAuthPersistence } from '../persistence/user-auth.persistence';
import { SiteReportService } from '../services/site-report.service';
import { SiteReportPersistence } from '../persistence/site-report.persistence';
import { RolePersistence } from '../persistence/role.persistence';
import { RoleService } from '../services/role.service';
import { NotificationPersistence } from '../persistence/notification.persistence';
import { NotificationService } from '../services/notification.service';

// Bottle definition.
const bottle: any = new Bottle();
const dbModels = models;

// USER persistence and service setup.
bottle.factory('UserPersistence', () => new UserPersistence(dbModels));
bottle.service(
  'UserService',
  UserService,
  'UserPersistence',
  'AuthService',
  'UserAuthPersistence',
  'SitePersistence',
  'CommentPersistence',
  'VotePersistence',
  'SubscriptionPersistence'
);

// AWS Manager.
bottle.service('AwsManager', AwsManager);

// SES Manager.
bottle.service('SesManager', SesManager);

// USER AUTH persistence setup.
bottle.factory('UserAuthPersistence', () => new UserAuthPersistence(models));

bottle.factory('NotificationPersistence', () => new NotificationPersistence(models));
bottle.service('NotificationService', NotificationService, 'NotificationPersistence');

// ROLE service setup.
bottle.factory('RolePersistence', () => new RolePersistence(models));
bottle.service('RoleService', RoleService, 'RolePersistence');

// AUTH service setup.
bottle.service('AuthService', AuthService, 'UserPersistence', 'UserAuthPersistence', 'SesManager', 'RolePersistence');

// REPORT service setup.
bottle.factory('SiteReportPersistence', () => new SiteReportPersistence(models));
bottle.service('SiteReportService', SiteReportService, 'SiteReportPersistence', 'SitePersistence');

// SITE persistence and service setup.
bottle.factory('SitePersistence', () => new SitePersistence(dbModels));
bottle.service(
  'SiteService',
  SiteService,
  'SitePersistence',
  'UserPersistence',
  'NotificationPersistence',
  'CommentPersistence',
  'VotePersistence'
);

bottle.factory('CommentPersistence', () => new CommentPersistence(models));
bottle.service(
  'CommentService',
  CommentService,
  'CommentPersistence',
  'VotePersistence',
  'NotificationPersistence',
  'SitePersistence'
);

// VOTE persistence and service setup.
bottle.factory(
  'VotePersistence',
  () => new VotePersistence(dbModels)
);
bottle.service(
  'VoteService',
  VoteService,
  'VotePersistence',
  'NotificationPersistence',
  'CommentPersistence',
  'SitePersistence'
);

// SUBSCRIPTION persistence and service setup.
bottle.factory('SubscriptionPersistence', () => new SubscriptionPersistence(models));
bottle.service(
  'SubscriptionService',
  SubscriptionService,
  'SubscriptionPersistence',
  'NotificationPersistence'
);

// Define the api and inject services into resolvers.
bottle.factory('api', container => (
  api(
    schema,
    resolvers(
      container
    ),
    container.AuthService
  )
)
);

const exportBottle: any = bottle;

// So the bottle can be renamed.
export default exportBottle;
