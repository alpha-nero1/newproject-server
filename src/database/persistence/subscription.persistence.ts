/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { PersistenceClass } from './persistence.class';

/**
 * @author Alessandro Alberga
 * @description persistence layer class for subscription operations.
 */
export class SubscriptionPersistence extends PersistenceClass {

  constructor(models: any) {
    super(models);
  }

  /**
   * Simple interfacing function to create a subscription.
   *
   * @param subscription subscription object.
   */
  public create(subscription: any): Promise<any> {
    return this.subscriptions.create(subscription);
  }

  /**
   * Delete (set disabled false) with params specified.
   * Facilitates unsubscription.
   *
   * @param userId user id
   * @param subscribeeId subscribee id.
   */
  public delete(userId: number, subscribeeId: number): Promise<any> {
    return this.subscriptions.update({ disabled: true }, {
      where: {
        user_id: userId,
        subscribee_user_id: subscribeeId
      },
      returning: true
    })
      .then(res => res[1]);
  }

  /**
   * Get the users who i have subscribed TO.
   *
   * @param userId user id.
   */
  public getMySubscribees(userId: number): Promise<any[]> {
    const query = {
      include: [
        { model: this.users, required: false, as: 'subscribee' }
      ],
      where: {
        subscriber_user_id: userId,
        disabled: false
      }
    };
    return this.subscriptions.findAndCountAll(query);
  }

  /**
   * Get the users who are subscribed to ME.
   *
   * @param userId id of user to get subscriptions.
   */
  public getMySubscribers(userId: number): Promise<any[]> {
    const query = {
      include: [
        { model: this.users, required: false, as: 'subscriber' }
      ],
      where: {
        subscribee_user_id: userId,
        disabled: false
      }
    };
    return this.subscriptions.findAndCountAll(query);
  }

  /**
   * Get count of all of users subscribers.
   *
   * @param userId id of user.
   */
  public getCountOfAllUserSubscribers(userId: number): Promise<number> {
    return this.subscriptions.count({
      where: {
        disabled: false,
        subscribee_user_id: userId
      }
    });
  }

  /**
   * Get count of all of users subscribers.
   *
   * @param userId id of user.
   */
  public getCountOfAllUserSubscribedTo(userId: number): Promise<number> {
    return this.subscriptions.count({
      where: {
        disabled: false,
        subscriber_user_id: userId
      }
    });
  }
}
