/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2019: The New Project.
 * All Rights Reserved.
 */

import Sequelize, { QueryTypes } from 'sequelize';
import { PersistenceClass } from './persistence.class';
import { Site } from '../../models/types/site.interface';
import { User } from '../../models/types/user.interface';
import { GeneralQueryOpts } from '../../schema/types/general-query-opts.interface';
import { DailyFeature } from '../../models/types/daily-feature.interface';
import Utils from '../../common/utils';
import { Constants } from '../../common/constants';
import { RowsAndCount } from '../../models/types/general/rows-and-count.interface';

const { Op } = Sequelize;

/**
 * @author Alessandro Alberga
 * @description persistence layer class for sites.
 */
export class SitePersistence extends PersistenceClass {
  constructor(models: any) {
    super(models);
  }

  /**
   * Create a site.
   *
   * @param site site to create
   */
  public create(site: any, transaction?: any): Promise<Site> {
    return this.sites.create(site, {
      returning: true,
      transaction
    });
  }

  /**
   * Update a site to new values.
   *
   * @param siteId id of the site to mutate.
   * @param newValues new values of the site.
   */
  public update(siteId: number, newValues: Partial<Site>): Promise<any> {
    return this.sites.update(newValues, {
      returning: true,
      where: { id: siteId }
    })
    .then(res => {
      if (res && res[1] && res[1][0]) {
        return res[1][0];
      }
    });
  }

  /**
   * Return a list of sites that a user has upvoted.
   *
   * @param userId id of the user.
   * @param page page of results to get.
   */
  public getSitesWithVotes(userId: number, up: boolean, page: number): Promise<any[]> {
    const queryLimit = 20;
    const queryOffset = page * queryLimit;
    const { sequelize } = this.users;
    // Get all of the users upvotes and match them directly to a unique site.
    // Tries to get a flat list back while using the site votes as the initial table.
    // for scalability sake.
    const queryModel = up ? this.site_upvotes : this.site_downvotes;
    return queryModel.findAll({
      attrbiutes: [
        'id',
        [sequelize.col('site_upvotes.sites.id'), 'site_id'],
        [sequelize.col('site_upvotes.sites.uuid'), 'uuid'],
        [sequelize.col('site_upvotes.sites.title'), 'title']
      ],
      where: {
        user_id: userId,
        disabled: false
      },
      include: [
        {
          model: this.sites
        }
      ],
      group: [
        // Group by site should make results distinct.
        'site_upvotes.sites.id'
      ],
      limit: queryLimit,
      offset: queryOffset
    });
  }

  /**
   * Get all sites essentially ordering by votes.
   *
   * @param queryOpts query options.
   */
  public getTrendingSites(queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    return this.getSitesWhere({}, queryOpts);
  }

  /**
   * Get reccommended sites for a user. Order by
   * votes.
   *
   * @param user user object.
   * @param queryOpts query options.
   */
  public findRecommendedSites(user: any, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    const { tags } = user;
    return this.getSitesWhere(
      {
        tags: {
          [Op.overlap]: tags
        }
      },
      queryOpts
    );
  }

  /**
   * Get a site by its corresponding uuid.
   *
   * @param uuid unique id of site.
   */
  public getSiteByUuid(uuid: string, contexUser: User): Promise<any> {
    // Define query first incase we want to log it before firing.
    const query = {
      where: { uuid },
      include: [
        {
          model: this.users,
          required: false,
          as: 'user',
        },
        {
          model: this.site_reports,
          required: false,
          as: 'reports',
          // Plug on only the site report/s made by the context user so that the front end can tell if this
          // user has already reported the site or not.
          where: {
            reporting_user_id: contexUser.id,
            dismissed: false
          }
        }
      ]
    };
    return this.sites.findOne(query);
  }

  /**
   * Get site by its id.
   *
   * @param siteId id of site querying for.
   * @param transaction potential transaction.
   */
  public getSiteById(siteId: number, transaction?: any): Promise<Site> {
    return this.sites.findOne({
      include: [
        {
          model: this.users,
          as: 'user'
        }
      ],
      where: {
        id: siteId,
        disabled: false
      },
      transaction
    });
  }

  /**
   * Search sites.
   *
   * @param keyword word to search for.
   * @param tags site tags to filter by.
   * @param queryOpts query opts.
   * @param transaction optional transaction.
   */
  public searchSites(
    keyword: string,
    tags: string[],
    queryOpts: GeneralQueryOpts,
    transaction?: any
  ): Promise<RowsAndCount<Site>> {
    let tagsWhere;
    if (tags && tags.length) {
      tagsWhere = { tags: { [Op.overlap]: tags } };
    }
    const where: any = {
      [Op.or]: {
        title: {
          [Op.iLike]: `%${keyword}%`
        },
        ...tagsWhere,
        'user.username': {
          [Op.iLike]: `%${keyword}%`
        },
        'user.first_name': {
          [Op.iLike]: `%${keyword}%`
        },
        'user.last_name': {
          [Op.iLike]: `%${keyword}%`
        }
      }
    };
    return this.getSitesWhere(where, queryOpts, transaction);
  }

  /**
   * List daily features.
   *
   * @param opts options for query.
   */
  public getDailyFeatures(opts: GeneralQueryOpts): Promise<DailyFeature[]> {
    return this.daily_features.findAndCountAll(Utils.PaginateResults({
      include: [{
        model: this.sites,
        as: 'site',
        include: [{
          model: this.users,
          as: 'user'
        }]
      }],
      order: [['created_at', 'DESC']]
    }, opts.page, opts.page_size));
  }

  /**
   * Create a new daily feature.
   *
   * @param feature feature to create.
   */
  public createDailyFeature(feature: DailyFeature): Promise<DailyFeature> {
    return this.daily_features.create(feature, { returning: true });
  }

  /**
   * Get count of user sites.
   *
   * @param userId user id.
   */
  public getCountOfUserSites(userId: number): Promise<number> {
    return this.sites.count({
      where: {
        disabled: false,
        passed_report_threshold: false,
        user_id: userId
      }
    });
  }

  /**
   * Get a users sites.
   *
   * @param userId id of user.
   * @param queryOpts query options.
   * @param transaction transaction.
   */
  public getUsersSites(
    userId: number,
    queryOpts: GeneralQueryOpts,
    transaction?: any
  ) {
    return this.getSitesWhere(
      {
        disabled: false,
        user_id: userId
      },
      queryOpts,
      transaction
    );
  }

  /**
   * Get a site with its votes.
   *
   * @param siteId id of site.
   * @param transaction transaction.
   */
  public getSiteWithVotes(siteId: number, transaction?: any): Promise<Site> {
    const { sequelize } = this.sites;
    return this.sites.findOne({
      attributes: [
        'id',
        'title',
        'user_id',
        'uuid',
        'created_at',
        [sequelize.fn('count', sequelize.col('upvotes.id')), 'upvotes_count'],
        [sequelize.fn('count', sequelize.col('downvotes.id')), 'downvotes_count']
      ],
      subQuery: false,
      include: [
        {
          attributes: [],
          model: this.site_upvotes,
          as: 'upvotes',
          required: false,
          where: {
            disabled: false
          }
        },
        {
          attributes: [],
          model: this.site_downvotes,
          as: 'downvotes',
          required: false,
          where: {
            disabled: false
          }
        }
      ],
      where: {
        id: siteId,
        disabled: false
      },
      group: [
        'sites.id',
        'sites.title',
        'sites.user_id',
        'sites.uuid',
        'sites.created_at',
      ],
      transaction
    })
    .then(res => {
      let processedRes = res;
      if (res) {
        processedRes = processedRes.dataValues;
        processedRes.upvotes = Utils.MapRecordToRowAndCount([], processedRes.upvotes_count);
        processedRes.downvotes = Utils.MapRecordToRowAndCount([], processedRes.downvotes_count);
      }
      return processedRes;
    });
  }

  /**
   * Get users sites.
   *
   * @param userId id of user.
   * @param queryOpts query options.
   * @param transaction transaction.
   */
  public getSitesWhere(
    where: any,
    queryOpts?: GeneralQueryOpts,
    transaction?: any
  ): Promise<RowsAndCount<Site>> {
    const { sequelize } = this.sites;
    return Promise.all([
      this.sites.count({
        where
      }),
      this.sites.findAll(Utils.PaginateResults({
        attributes: [
          'id',
          'title',
          'user_id',
          'uuid',
          'created_at',
          'banner_photo',
          [sequelize.fn('count', sequelize.col('upvotes.id')), 'upvotes_count'],
          [sequelize.fn('count', sequelize.col('downvotes.id')), 'downvotes_count']
        ],
        subQuery: false,
        include: [
          {
            attributes: ['id', 'first_name', 'last_name', 'username', 'profile_photo'],
            model: this.users,
            as: 'user',
            required: false
          },
          {
            attributes: [],
            model: this.site_upvotes,
            as: 'upvotes',
            required: false
          },
          {
            attributes: [],
            model: this.site_downvotes,
            as: 'downvotes',
            required: false
          }
        ],
        group: [
          // NOTE: Put proper pks in place to avoid this
          'sites.id',
          'sites.title',
          'sites.user_id',
          'sites.uuid',
          'sites.created_at',
          'sites.banner_photo',
          'user.id',
          'user.first_name',
          'user.last_name',
          'user.username',
          'user.profile_photo'
        ],
        where,
        transaction
    }, queryOpts?.page, queryOpts?.page_size))
  ])
    .then(([count, rows]) => ({
        count,
        rows: rows.map(row => {
          const retRow = row.dataValues;
          retRow.upvotes = Utils.MapRecordToRowAndCount([], retRow.upvotes_count);
          retRow.downvotes = Utils.MapRecordToRowAndCount([], retRow.downvotes_count);
          return retRow;
        })
      }));
  }
}
