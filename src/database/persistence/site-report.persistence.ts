/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { PersistenceClass } from './persistence.class';
import { ListReportedSitesParams } from '../../schema/types/list-reported-sites-params.interface';
import { Site } from '../../models/types/site.interface';
import { Op } from 'sequelize';
import { SiteReport } from '../../models/types/site-report.interface';

/**
 * @author Alessandro Alberga
 * @description Site report persistence file.
 */
export class SiteReportPersistence extends PersistenceClass {
  constructor(model) {
    super(model);
  }

  /**
   * Get reported sites by provided params.
   *
   * @param opts options to query by.
   */
  public getReportedSitesByParams(opts: ListReportedSitesParams): Promise<Site[]> {
    const siteWhere: any = { where: { } };
    const reportWhere: any = {
      where: {
        dismissed: false
      }
    };
    // Consider the options in the where statement.
    if (opts.after_date) {
      siteWhere.where.created_at = { [Op.gt]: opts.after_date };
    }
    if (opts.before_date) {
      siteWhere.where.created_at = {
        ...siteWhere.where.created_at,
        [Op.lt]: opts.before_date
      };
    }
    if (opts.site_passed_threshold) {
      siteWhere.where.passed_report_threshold = opts.site_passed_threshold;
    }
    // Bombs away...
    return this.sites.findAndCountAll({
      include: [
        {
          model: this.users,
          required: false,
          as: 'user'
        },
        {
          model: this.site_reports,
          as: 'reports',
          ...reportWhere
        }
      ],
      ...siteWhere
    });
  }

  /**
   * Find all reports by options.
   *
   * @param opts possible options to restrict set by.
   * @param transaction potential transaction.
   */
  public findAllByOptions(opts: ListReportedSitesParams, transaction?: any): Promise<SiteReport[]> {
    const queryOptions: any = {
      where: {
        disabled: false,
        dismissed: false
      }
    };
    if (opts.site_id) {
      queryOptions.where.site_id = opts.site_id;
    }
    if (opts.dismissed) {
      queryOptions.where.dismissed = opts.dismissed;
    }
    queryOptions.include = [
      {
        model: this.users,
        as: 'reporting_user'
      },
      {
        model: this.sites,
        as: 'site'
      }
    ];
    return this.site_reports.findAndCountAll(queryOptions, { transaction });
  }

  /**
   * Report a site.
   *
   * @param siteId id of site to report
   * @param reason reason of report.
   * @param reportingUserId id of reporting user.
   * @param transaction potential transaction.
   */
  public reportSite(siteId: number, reason: string, reportingUserId: number, transaction?: any): Promise<any> {
    return this.site_reports.create(
      {
        site_id: siteId,
        reason,
        reporting_user_id: reportingUserId
      },
      {
        returning: true,
        transaction
      }
    );
  }

  /**
   * Dismiss a site report.
   *
   * @param siteReportIds id of site reports.
   * @param transaction potential transaction.
   */
  public dismissReports(siteReportIds: number[], transaction?: any): Promise<any> {
    return this.site_reports.update(
      {
        dismissed: true
      },
      {
        where: {
          id: {
            [Op.in] : siteReportIds
          },
          disabled: false
        },
        transaction,
        returning: true
      }
    )
    .then(([success, res]) => res);
  }
}
