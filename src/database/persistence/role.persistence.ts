/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { User } from 'src/models/types/user.interface';
import { PersistenceClass } from './persistence.class';
import { Operation } from '../../models/types/operation.interface';
import { Role } from '../../models/types/role.interface';

/**
 * @author Alessandro Alberga
 * @description Role persistence managing permission access checks.
 */
export class RolePersistence extends PersistenceClass {
  constructor(models) {
    super(models);
  }

  /**
   * Get all function for roles.
   */
  public getAllRoles(): Promise<Role[]> {
    return this.roles.findAll();
  }

  /**
   * Assign roles to a user.
   *
   * @param userId id of user.
   * @param roleBits role bits value.
   */
  public assignRolesToUser(userId: number, roleBits: number): Promise<User> {
    return this.users.update(
      { role_bits: roleBits },
      { returning: true, where: { id: userId, disabled: false } }
    ).then(([success, res]) => res[0]);
  }

  /**
   * Verifies that a user has access to operations specified. If returns true,
   * the user may proceed.
   *
   * @param user context user object.
   * @param operations operation names.
   */
  public verifyOperationsAccess(user: User, operations: string[]): Promise<boolean> {
    return this.operations.findAll({
      where: {
        disabled: false,
        operation: operations
      }
    })
    .then((foundOps: Operation[]) => {
      // tslint:disable-next-line: no-bitwise
      return foundOps.every(op => ((op.role_bits & user.role_bits) !== 0));
    });
  }
}
