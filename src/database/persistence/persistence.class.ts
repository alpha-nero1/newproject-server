/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description persistence base class that loads models so this work doesn't
 *   need to be done all the time.
 */
export class PersistenceClass {
  // tslint:disable: variable-name
  /**
   * comments model.
   */
  public comments: any;

  /**
   * Users model.
   */
  public users: any;

  /**
   * Sites model.
   */
  public sites: any;

  /**
   * Site downvotes model.
   */
  public site_downvotes: any;

  /**
   * Site upvotes model.
   */
  public site_upvotes: any;

  /**
   * Comment upvotes model.
   */
  public comment_upvotes: any;

  /**
   * Comment downvotes model.
   */
  public comment_downvotes: any;

  /**
   * Subscriptions model.
   */
  public subscriptions: any;

  /**
   * User auths model.
   */
  public user_auths: any;

  /**
   * User auth reset model.
   */
  public user_auth_resets: any;

  /**
   * User auth token model.
   */
  public user_auth_tokens: any;

  /**
   * Operations model.
   */
  public operations: any;

  /**
   * Site reports model.
   */
  public site_reports: any;

  /**
   * Role model.
   */
  public roles: any;

  /**
   * Daily feature.
   */
  public daily_features: any;

  /**
   * Notifications model.
   */
  public notifications: any;

  /**
   * Site comments link table model.
   */
  public site_comments: any;

  constructor(modelsObject: any) {
    this.loadModels(modelsObject);
  }

  /**
   * Load models so child classes have easy access.
   *
   * @param modelsObject all models loaded in the database we called
   *   super() with in the extending class.
   */
  private loadModels(modelsObject: any): void {
    Object.keys(modelsObject).forEach(key => {
      this[key] = modelsObject[key];
    });
  }

  /**
   * Provide a transaction for persistence + service classes.
   */
  public transaction(callback: (transaction) => Promise<any>) {
    const { sequelize } = this.users;
    return sequelize.transaction(callback);
  }
}
