/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { PersistenceClass } from './persistence.class';
import uuid from 'uuid/v4';
import { UserAuth } from '../../models/types/user-auth.interface';
import { Constants } from '../../common/constants';
import Utils from '../../common/utils';

/**
 * @author Alessandro Alberga
 * @description user auth persistence class.
 */
export class UserAuthPersistence extends PersistenceClass {
  constructor(models: any) {
    super(models);
  }

  /**
   * Create new user auth record.
   *
   * @param userAuthToCreate user auth to create.
   */
  public createUserAuth(userAuthToCreate: UserAuth): Promise<any> {
    return this.user_auths.create(userAuthToCreate, { returning: true });
  }

  /**
   * Update user auth.
   *
   * @param userAuthId user auth id to mutate.
   * @param newValues new values to give record.
   */
  public updateUserAuth(userAuthId: number, newValues: UserAuth): Promise<any> {
    return this.user_auths.update({ ...newValues }, { where: { id: userAuthId, disabled: false } });
  }

  /**
   * Soft delete a user auth.
   *
   * @param userAuthId id of user auth to soft delete.
   */
  public deleteUserAuth(userAuthId: number): Promise<any> {
    return this.user_auths.update({ where: { id: userAuthId } }, { disabled: true });
  }

  /**
   * Find user auth reset by reset token.
   *
   * @param resetToken find token by reset.
   */
  public findUserAuthReset(resetToken: string): Promise<any> {
    return this.user_auth_resets.findOne({
      include: [
        // User auth to then call update if reset successfull.
        { model: this.user_auths, required: false, as: 'user_auth' }
      ],
      where: {
        reset_token: resetToken,
        disabled: false
      }
    });
  }

  /**
   * Create a new user auth reset, so long as there is no prior reset of half minute ago.
   *
   * @param userAuthId id to make user auth reset for.
   */
  public createUserAuthReset(userAuthId: number): Promise<any> {
    const dateSeconds = new Date().setSeconds(new Date().getSeconds());
    const updatedAt = new Date(dateSeconds - Constants.authResetThrottle);

    return new Promise(resolve => {
      this.user_auth_resets.findOne({
        where: {
          created_at: {
            $gt: updatedAt
          },
          disabled: false
        }
      })
        .then(userAuthReset => {
          if (userAuthReset) {
            resolve({ success: false, reason: 'Reset throttle' });
          } else {
            // Create auth reset with token and expiry.
            this.user_auth_resets.create({
              user_auth_id: userAuthId,
              reset_token: uuid(),
              expiry_date: Utils.getAuthResetExpiryDate()
            }, { returning: true })
              .then((uatReset) => {
                resolve({ success: true, reset_token: uatReset.reset_token });
              });
          }
        });
    });
  }

  /**
   * Find user auth by user handle.
   *
   * @param handle handle of user authentication.
   */
  public findUserAuthByHandle(handle: string): Promise<any> {
    return this.user_auths.findOne({ where: { handle, disabled: false } });
  }

  /**
   * Find a user auth by its id.
   *
   * @param id id of user auth to fetch.
   */
  public findUserAuthById(id: number): Promise<UserAuth> {
    return this.user_auths.findOne({ where: { id, disabled: false } });
  }

  /**
   * Find user auth by user handle.
   *
   * @param handle handle of user authentication.
   */
  public findUserAuthsByHandles(handles: string[]): Promise<any[]> {
    return this.user_auths.findAll({ where: { handle: handles, disabled: false } });
  }

  /**
   * Simply update token expiry date.
   *
   * @param token auth token
   * @param byDays days to increment expiry by.
   */
  public updateTokenExpiry(token: string): Promise<any> {
    return this.user_auth_tokens.update(
      { expiry_date: Utils.getNewAuthTokenExpiryDate()}, { where: { token } });
  }

  /**
   * See if a user has a valid user auth token, if so return it, otherwise return false.
   *
   * @param userAuthId id of user auth to evaluate.
   */
  public findMyValidUserAuthToken(userAuthId: number): Promise<any | boolean> {
    return new Promise((resolve) => {
      this.user_auth_tokens.findOne({
        where: {
          user_auth_id: userAuthId,
          expiry_date: {
            $gt: new Date()
          }
        }
      })
        .then(userAuthToken => {
          if (userAuthToken) {
            resolve(userAuthToken);
          } else {
            resolve(false);
          }
        });
    });
  }

  /**
   * Create a new user auth token for a user.
   *
   * @param user_id id of user to create user auth token for.
   */
  public createUserAuthToken(userAuthId: number): Promise<any> {
    return this.user_auth_tokens.create({
      user_auth_id: userAuthId,
      expiry_date: Utils.getNewAuthTokenExpiryDate(),
      token: uuid(),
      created_at: new Date(),
      updated_at: new Date(),
      disabled: false
    }, { returning: true });
  }
}
