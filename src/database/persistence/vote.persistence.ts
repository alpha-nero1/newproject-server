/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { PersistenceClass } from './persistence.class';
import { QueryTypes } from 'sequelize';

/**
 * @author Alessandro Alberga
 * @description persistence level class for voting functionality.
 */
export class VotePersistence extends PersistenceClass {

  constructor(models: any) {
    super(models);
  }

  /**
   * Get all up and downvotes for a site (count and limited results).
   *
   * @param siteId id of site to fetch votes with.
   */
  public async getSiteVotes(siteId: number, transaction?: any): Promise<any> {
    const conditions = {
      where: {
        site_id: siteId,
        disabled: false
      },
      transaction
    };
    // Format votes.
    const upvotes = await this.site_upvotes.findAndCountAll(conditions);
    const downvotes = await this.site_downvotes.findAndCountAll(conditions);
    return {
      upvotes: {
        count: upvotes?.count,
        rows: upvotes?.rows
      },
      downvotes: {
        count: downvotes?.count,
        rows: downvotes?.rows
      }
    };
  }

  /**
   * Get all up and downvotes for a comment (count and limited results)
   *
   * @param commentId comment id.
   */
  public async getCommentVotes(commentId: number, transaction?: any): Promise<any> {
    const conditions = {
      where: {
        comment_id: commentId,
        disabled: false
      },
      limit: 5,
      transaction
    };
    const upvotes = await this.comment_upvotes.findAndCountAll(conditions);
    const downvotes = await this.comment_downvotes.findAndCountAll(conditions);
    return {
      upvotes: {
        count: upvotes?.count,
        rows: upvotes?.rows
      },
      downvotes: {
        count: downvotes?.count,
        rows: downvotes?.rows
      }
    };
  }

  /**
   * Vote on a site.
   *
   * @param siteId id of site to add vote to.
   * @param userId id of user who made vote.
   * @param up 0 if downvote, else upvote.
   */
  public async voteOnSite(siteId: number, userId: number, up: boolean, transaction?: any): Promise<any> {
    // Form the vote object.
    const voteToSave = { site_id: siteId, user_id: userId };
    // Execute vote stepper.
    return this.voteStepper(
      this.site_upvotes,
      this.site_downvotes,
      voteToSave,
      up,
      transaction
    );
  }

  /**
   * Vote on a comment.
   *
   * @param commentId comment id to add vote.
   * @param userId user who voted.
   * @param up 0 if downvote, else upvote.
   */
  public async voteOnComment(commentId: number, userId: number, up: boolean, transaction?: any): Promise<any> {
    // Form the comment vote object.
    const voteToSave = { comment_id: commentId, user_id: userId };
    // Execute vote stepper.
    return this.voteStepper(
      this.comment_upvotes,
      this.comment_downvotes,
      voteToSave,
      up,
      transaction
    );
  }

  /**
   * Vote stepper. performs the actual voting. It does several things:
   * It makes sure double voting cannot happen, it removes a downvote record from
   * the model if upvoting and it exists and vice versa. It allows the models (site/comment/reply)
   * to step between votes of 1 0 and -1.
   *
   * @param upModel upvoting model table to use.
   * @param downModel downvoting model table to use.
   * @param voteToSave vote to save (used to create vote and to search for records).
   * @param up execute upvote or downvote option.
   */
  private voteStepper(
    upModel: any,
    downModel: any,
    voteToSave: any,
    up: boolean,
    transaction?: any
  ): Promise<any> {
    const voteRecord = {
      ...voteToSave,
      disabled: false
    };
    // Firstly disable all up and downs with the save creds.
    return Promise.all([
      upModel.update({ disabled: true }, { where: voteRecord, returning: true, transaction }),
      downModel.update({ disabled: true }, { where: voteRecord, returning: true, transaction })
    ])
    .then(([upvoteRes, downvoteRes]) => {
      const wasPriorUpvoted = !!upvoteRes[1]?.length;
      const wasPriorDownvoted = !!downvoteRes[1]?.length;
      // Create the new vots record based on if we want up or down!
      if (up && !wasPriorDownvoted) {
        return upModel.create(voteRecord, { transaction });
      }
      if (!up && !wasPriorUpvoted) {
        return downModel.create(voteRecord, { transaction });
      }
      return Promise.resolve(0);
    });
  }

  /**
   * Run a raw query to get the total count of site upvotes or downvotes that a user has
   * on their sites.
   *
   * @param userId user id.
   * @param up up or down votes.
   */
  public getTotalAccumulatedVotes(userId: number, up: boolean): Promise<number> {
    const votesTable = up ? 'site_upvotes' : 'site_downvotes';
    return this.sites.sequelize.query(
      `
      SELECT
        COUNT(votes.id) as total_votes
      FROM sites s
      LEFT JOIN ${votesTable} votes ON votes.site_id = s.id
      WHERE (
        s.user_id = ? AND
        s.disabled = false AND
        s.passed_report_threshold = false
      );
      `,
      {
        replacements: [
          userId
        ],
        type: QueryTypes.SELECT
      }
    )
    .then(res => {
      if (res && res[0]) {
        return res[0].total_votes;
      }
      return 0;
    });
  }
}
