/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Op, Transaction } from 'sequelize';
import { PersistenceClass } from './persistence.class';
import { Comment } from '../../models/types/comment.interface';
import { GeneralQueryOpts } from '../../schema/types/general-query-opts.interface';
import Utils from '../../common/utils';
import { RowsAndCount } from '../../models/types/general/rows-and-count.interface';

const VoteRecordLimit = 5;

/**
 * @author Alessandro Alberga
 * @description persistence layer handling commenting on all entities.
 */
export class CommentPersistence extends PersistenceClass {

  constructor(models: any) {
    super(models);
  }

  /**
   * Get the general options that are used to get comments.
   *
   * @param userId contextual id of user.
   * @param additionalIncludes any additional includes we would like.
   * @param transaction potential transaction inclusion.
   */
  private getGeneralCommentsOptions = (userId, additionalIncludes = [], transaction?) => ({
    include: [
      ...additionalIncludes,
      { model: this.users, required: false, as: 'user' },
      {
        attributes: [
          'id',
          [
            this.comments.sequelize.literal(`
              (SELECT COUNT(cv.id) FROM
              comment_upvotes cv WHERE
              cv.comment_id = "comments".id AND
              cv.user_id = ${userId} AND
              cv.disabled = false)
            `), 'count'
          ]
        ],
        model: this.comment_upvotes,
        required: false,
        where: { disabled: false },
        as: 'upvotes',
      },
      {
        attributes: [
          'id',
          [
            this.comments.sequelize.literal(`
              (SELECT COUNT(cd.id) FROM
              comment_downvotes cd WHERE
              cd.comment_id = "comments".id AND
              cd.user_id = ${userId} AND
              cd.disabled = false)
            `), 'count'
          ]
        ],
        model: this.comment_downvotes,
        required: false,
        where: { disabled: false },
        as: 'downvotes',
      }
    ],
    order: [
      ['created_at', 'DESC']
    ],
    transaction
  })

  /**
   * Simple find the comment record by id.
   *
   * @param id id of comment.
   */
  public findByPk(id: number): Promise<Comment> {
    return this.comments.findOne({
      where: {
        id
      }
    });
  }

  /**
   * Get count of all replies of a comment.
   *
   * @param commentId id of comment.
   */
  public getCountOfAllReplies(commentId: number): Promise<number> {
    return this.comments.count({
      where: {
        parent_comment_id: commentId
      }
    });
  }

  /**
   * Find all comments under a site.
   *
   * @param siteId id of a site.
   * @param userId contextual user id.
   * @param queryOpts query options.
   * @param transaction optional transaction.
   */
  public findCommentsBySite(
    siteId: number,
    userId: number,
    queryOpts: GeneralQueryOpts,
    transaction?: any
  ): Promise<RowsAndCount<Comment>> {
    return this.comments.findAndCountAll(Utils.PaginateResults({
      ...this.getGeneralCommentsOptions(
        userId,
        [{
          attributes: [],
          model: this.site_comments,
          as: 'dummy_middleman_association',
          where: {
            site_id: siteId
          }
        }],
        transaction
      ),
      where: {
        disabled: false
      }
    }, queryOpts?.page, queryOpts?.page_size))
    .then(res => ({
      ...res,
      rows: this.mapCommentsToThoseWithCorrectVoteRepsonses(res.rows)
    }));
  }

  /**
   * Takes comment ids, generally that of a page to refresh and gets new values from the DB.
   *
   * @param ids ids to refresh.
   * @param userId contextual user id.
   * @param queryOpts query options, sort value here is important.
   */
  public findCommentsByIds(ids: number[], userId: number, queryOpts: GeneralQueryOpts): Promise<Comment[]> {
    if (ids?.length > 100) {
      throw new Error('**ERROR: You ask for too much.');
    }
    return this.comments.findAll({
      ...this.getGeneralCommentsOptions(userId),
      where: {
        disabled: false,
        id: {
          [Op.in]: ids
        }
      },
      order: [
        ['created_at', (queryOpts?.sort_by_value || 'DESC')]
      ]
    })
    .then(comments => (
      this.mapCommentsToThoseWithCorrectVoteRepsonses(comments)
    ));
  }

  /**
   * List comments that have the parent id of the comment id sent in.
   *
   * @param commentId parent comment id.
   * @param userId id of acting user.
   * @param queryOpts query options.
   * @param transaction optional transaction.
   */
  public findRepliesByCommentId(
    commentId: number,
    userId: number,
    queryOpts?: GeneralQueryOpts,
    transaction?: any
  ): Promise<RowsAndCount<Comment>> {
    const { sequelize } = this.comments;
    return this.comments.findAndCountAll(Utils.PaginateResults({
      ...this.getGeneralCommentsOptions(userId, [], transaction),
      where: {
        parent_comment_id: commentId,
        disabled: false
      },
      order: [
        ['created_at', 'ASC']
      ],
    }, queryOpts?.page, queryOpts?.page_size))
    .then(res => ({
      ...res,
      rows: this.mapCommentsToThoseWithCorrectVoteRepsonses(res.rows)
    }));
  }

  /**
   * Get comment by its id
   *
   * @param commentId comment id.
   * @param transaction potential transaction.
   */
  public findCommentById(commentId: number, transaction?: any): Promise<Comment> {
    return this.comments.findOne({
      include: [
        {
          model: this.users,
          required: false,
          as: 'user'
        }
      ],
      where: {
        id: commentId,
        disabled: false
      },
      transaction
    });
  }

  /**
   * Take a comment array and repair the up and down votes, returns a mapped copy
   * of the array passed in.
   *
   * @param comments the comments to transform.
   */
  private mapCommentsToThoseWithCorrectVoteRepsonses = (comments: Comment[]) => (
    comments.map((comment: any) => {
      const retComment = comment;
      retComment.upvotes = {
        count: +(
          retComment?.upvotes?.length ?
          retComment.upvotes[0]?.dataValues?.count :
          0
        ),
        rows: []
      };
      retComment.downvotes = {
        count: +(
          retComment?.downvotes?.length ?
          retComment.downvotes[0]?.dataValues?.count :
          0
        ),
        rows: []
      };
      return retComment;
    })
  )

  /**
   * Comment on a site.
   *
   * @param comment comment object
   * @param transaction optional transaction.
   */
  public comment(comment: Partial<Comment>, transaction?: Transaction): Promise<Comment  > {
    return this.comments.create(comment, { transaction, returning: true });
  }

  /**
   * Delete a comment by its id.
   *
   * @param commentId id of comment to delete.
   */
  public deleteCommentById(commentId: number): Promise<any> {
    return this.comments.update({ disabled: true }, {
      where: {
        id: commentId
      },
      returning: true
    }).then(res => res[1][0]);
  }

  // Not sure about this...
  public cascadeDeleteOnReplies(commentId: number): Promise<any> {
    return this.comments.update({ disabled: true }, {
      where: {
        parent_comment_id: commentId
      }
    });
  }

  /**
   * Get a count of all the comments that a user has ever made.
   *
   * @param userId user id.
   */
  public getCountOfUsersComments(userId: number): Promise<number> {
    return this.comments.count({
      where: {
        disabled: false,
        user_id: userId
      }
    });
  }
}
