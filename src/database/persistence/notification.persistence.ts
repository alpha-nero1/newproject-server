/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { PersistenceClass } from './persistence.class';
import { Notification, NotificationQueryOpts } from '../../models/types/notification/notification.interface';
import Utils from '../../common/utils';
import { Op } from 'sequelize';

/**
 * @author Alessandro Alberga
 * @description persistence class managing user activities.
 */
export class NotificationPersistence extends PersistenceClass {
  constructor(models) {
    super(models);
  }

  /**
   * Create a user activity record.
   *
   * @param activity activity record to create.
   * @param opts options to insert into create.
   */
  public create(activity: Notification, sequelizeOpts?: any): Promise<Notification> {
    return this.notifications.create(
      activity,
      {
        returning: true,
        ...sequelizeOpts
      }
    );
  }

  /**
   * Get notifications.
   *
   * @param queryOpts general query options.
   * @param sequelizeOpts sequelize options to include. e.g transaction.
   */
  public getNotifications(queryOpts: NotificationQueryOpts, sequelizeOpts?: any): Promise<{ count: number, rows: Notification[] }> {
    return this.notifications.findAndCountAll(
      Utils.PaginateResults(
        {
          where: {
            user_id: queryOpts.user_id
          },
          order: [['created_at', 'DESC']]
        },
        queryOpts.page,
        queryOpts.page_size
      ),
      sequelizeOpts
    );
  }

  /**
   * Read notifications.
   *
   * @param notificationIds notification ids.
   * @param userId user id.
   */
  public readNotifications(notificationIds, userId): Notification[] {
    return this.notifications.update({ read: true }, {
      where: {
        user_id: userId,
        id: {
          [Op.in]: notificationIds
        }
      },
      returning: true
    });
  }
}
