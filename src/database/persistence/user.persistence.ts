/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Op } from 'sequelize';
import Utils from '../../common/utils';
import { PersistenceClass } from './persistence.class';
import { User } from '../../models/types/user.interface';

/**
 * @author Alessandro Alberga
 * @description persistence layer operations class for users.
 */
export class UserPersistence extends PersistenceClass {

  constructor(models: any) {
    super(models);
  }

  /**
   * Get all admin users.
   */
  public getAdminUsers(): Promise<User[]> {
    return this.users.findAll({
      where: {
        disabled: false,
        role_bits: {
          [Op.ne]: 0
        }
      }
    });
  }

  /**
   * Find a user given a user auth token.
   *
   * @param userAuthToken user auth token to retrieve user with.
   */
  public findUserByUserAuthToken(userAuthToken: string): Promise<any> {
    const { sequelize } = this.users;
    return this.users.findOne({
      include: [
        {
          model: this.user_auths,
          include: [
            {
              model: this.user_auth_tokens,
              where: {
                token: userAuthToken,
                expiry_date: {
                  [Op.gt]: sequelize.fn('NOW')
                }
              }
            }
          ]
        }
      ]
    });
  }

  /**
   * Create a user from given data.
   *
   * @param userData user to create.
   */
  public create(userData: any): Promise<any> {
    return this.users.create(userData, { returning: true });
  }

  /**
   * Update a user.
   *
   * @param userId id of user to update
   * @param userData data of user to update.
   */
  public update(userId: number, userData: any): Promise<any> {
    return this.users.update(userData, {
      returning: true,
      where: { id: userId }
    }).then(res => res[1][0]);
  }

  /**
   * Take parameters and return matching users.
   *
   * @param firstName first name of user to find.
   * @param lastName last name of user to find.
   * @param email email of user to find.
   */
  public find(firstName: string, lastName: string, email: string): Promise<any> {
    const where: any = {};
    // Check for and add first name to query.
    if (firstName) {
      where.first_name = firstName;
    }

    // Check for and add last name to query.
    if (lastName) {
      where.last_name = lastName;
    }

    // Check for and add email to query.
    if (email) {
      where.email = email;
    }

    return this.users.findAll({
      where
    });
  }

  /**
   * Find a full user by id including related models.
   *
   * @param username username of user.
   */
  public findUserByUsername(username: string): Promise<any> {
    return this.users.findOne({
      where: { username }
    });
  }

  /**
   * Find a user with email.
   *
   * @param email email of user.
   */
  public findOne(email: string): Promise<any> {
    return this.users.findOne({
      where: {
        email
      }
    });
  }

  /**
   * Retrieve a user by its id and include the subscriptions they have made.
   *
   * @param userId id of user to find.
   */
  public findOneById(userId: number): Promise<any> {
    const query = {
      include: [
        {
          model: this.subscriptions,
          required: false,
          as: 'subscriptions',
          where: { disabled: false },
        },
        {
          model: this.subscriptions,
          required: false,
          as: 'subscribers',
          where: { disabled: false },
        }
      ],
      where: { id: userId, disabled: false }
    };
    return this.users.findOne(query);
  }

  /**
   * Do a find all on users with one specific id. We are only ever expecting one
   * result but we do a find all because otherwise the query that sequelize geenrates does
   * not allow us to include a user model again.
   *
   * @param userId id of user to find.
   */
  public findAllOfMeById(userId: number): Promise<any> {
    const query = {
      include: [
        {
          model: this.subscriptions,
          required: false,
          as: 'subscriptions',
          where: { disabled: false },
          include: [
            { model: this.users, required: false, as: 'subscribee' }
          ]
        },
        {
          model: this.subscriptions,
          required: false,
          as: 'subscribers',
          where: { disabled: false },
          include: [
            { model: this.users, required: false, as: 'subscriber' }
          ]
        }
      ],
      where: { id: userId, disabled: false }
    };
    return this.users.findAll(query)
      .then(res => {
        const user = res[0];
        user.subscriptions = Utils.ArrayToRowsAndCount(user.subscriptions);
        user.subscribers = Utils.ArrayToRowsAndCount(user.subscribers);
        return user;
      });
  }

  /**
   * Find a user by username.
   *
   * @param username username of user.
   */
  public findByUsername(username: string): Promise<User> {
    return this.users.findOne({
      where: {
        username
      }
    });
  }
}
