/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import STS from 'aws-sdk/clients/sts';
// Get config vars
import { aws } from '../../config/config.json';

/**
 * @author Alessandro Alberga
 * @description This file will manage our aws permission endpoints for buckets.
 */
export class AwsManager {

  /**
   * Sts reference.
   */
  private sts: STS;

  constructor() {
    this.sts = new STS({
      accessKeyId: aws.sts.access_key_id,
      secretAccessKey: aws.sts.secret_access_key,
      region: aws.sts.region
    });
  }

  /**
   * A user wants to upload a profile picture. give them temporary
   * credentials to do so.
   */
  public getTemporaryCredentials(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.sts.getSessionToken(
        { DurationSeconds: 900 },
        (err, data) => {
          if (err) { reject(err); }
          // Define gql return.
          resolve(data.Credentials);
        });
    });
  }
}
