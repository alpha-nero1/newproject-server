/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import SES, { SendEmailRequest } from 'aws-sdk/clients/ses';
import { aws } from '../../config/config.json';
import { EmailTemplate } from '../../models/types/email-template.interface';
import { TemplateService } from '../templating/template.service';

/**
 * @author Alessandro Alberga
 * @description Ses manager for sending email.
 */
export class SesManager {
  /**
   * Ses reference.
   */
  private ses: SES;

  constructor() {
    this.ses = new SES({
      accessKeyId: aws.ses.access_key_id,
      secretAccessKey: aws.ses.secret_access_key,
      region: aws.ses.region
    });
  }

  /**
   * Send an email using SES.
   *
   * @param recipients array of recipient addresses.
   * @param subject subject of email.
   * @param emailTemplate email template to send.
   */
  public async sendSupportEmail(recipients: string[], subject: string, emailTemplate: EmailTemplate): Promise<any> {
    // Compile email.
    const compiledEmail = await TemplateService.CompileTemplate(emailTemplate);

    // Collect params.
    const params: SendEmailRequest = {
      Message: {
        Subject: {
          Charset: 'UTF-8',
          Data: subject
        },
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: compiledEmail
          },
          Text: {
            Charset: 'UTF-8',
            Data: Object.values(emailTemplate.template_data).join(', ')
          }
        }
      },
      Source: 'ale.alberga1@gmail.com',
      Destination: {
        ToAddresses: [...recipients]
      }
    };
    // Shoot email.
    return this.ses.sendEmail(params).promise();
  }
}
