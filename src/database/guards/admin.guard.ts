/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { User } from '../../models/types/user.interface';
import { AppRole } from './app-role';

/**
 * The admin guard to keep api's secure.
 *
 * @param contextUser context user.
 * @param successCallback callback to execute on success.
 */
export const AdminGuard = (contextUser: User, adminBit: AppRole, successCallback: () => Promise<any>) => {
  // tslint:disable-next-line: no-bitwise
  if ((contextUser.role_bits & adminBit) >  0) {
    return successCallback();
  }
  throw new Error('**ERR: Insufficient user access.');
};
