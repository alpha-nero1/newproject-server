/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { User } from '../../models/types/user.interface';

/**
 * Verify that what ever object is to be mutated in the backend is being done by the uyser themselves
 * and not anyone else.
 *
 * @param contextUser context user.
 * @param objectToCheck object containing user_id to check against context.
 * @param successCallback callback to execute on success.
 */
export const ActingUserGuard = (contextUser: User, objectToCheck: { user_id?: number }, successCallback: () => Promise<any>) => {
  // tslint:disable-next-line: no-bitwise
  if (contextUser?.id === objectToCheck?.user_id) {
    return successCallback();
  }
  throw new Error('**ERR: Invalid user.');
};
