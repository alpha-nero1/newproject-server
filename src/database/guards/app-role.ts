/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @authour Alessandro Alberga
 * @description simple enum describing the role bits for each role.
 */
export enum AppRole {
  Admin = 1,
  SuperAdmin = 2,
  Emperor = 4
}
