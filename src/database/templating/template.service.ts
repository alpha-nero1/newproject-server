/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { EmailTemplate } from '../../models/types/email-template.interface';
import { EmailTemplates } from './email-templates';

/**
 * @author Alessandro Alberga
 * @description service for templating functions.
 */
export class TemplateService {

  /**
   * Compile the template, inject the data and give back html.
   *
   * @param template email template to process.
   */
  public static CompileTemplate(template: EmailTemplate): Promise<string> {
    return EmailTemplates[template.template_name](template.template_data);
  }
}
