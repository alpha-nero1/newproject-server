/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

/**
 * @author Alessandro Alberga
 * @description password reset email template.
 */
export default (data: PasswordResetData) => (`
  <html>
    <body>
      <div>
        <h1>${data.title}</h1>
        <p>${data.reset_text}</p>
        <p>${data.reset_token}</p>
      </div>
    </body>
  </html>
`);

interface PasswordResetData {
  title: string;
  reset_text: string;
  reset_token: string;
}
