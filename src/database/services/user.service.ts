/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { UserPersistence } from '../persistence/user.persistence.js';
import { AuthService } from './auth.service.js';
import { UserAuthPersistence } from '../persistence/user-auth.persistence.js';
import { User } from '../../models/types/user.interface.js';
import { Roles } from './role.service.js';
import AppErrors from '../../common/appErrors.js';
import { UserStats } from '../../models/types/user-stats.interface.js';
import { CommentPersistence } from '../persistence/comment.persistence.js';
import { VotePersistence } from '../persistence/vote.persistence.js';
import { SitePersistence } from '../persistence/site.persistence.js';
import { SubscriptionPersistence } from '../persistence/subscription.persistence.js';

/**
 * @author Alessandro Alberga
 * @description service level class for user functionality.
 */
export class UserService {
  constructor(
    private userPersistence: UserPersistence,
    private authService: AuthService,
    private userAuthPersistence: UserAuthPersistence,
    private sitePersistence: SitePersistence,
    private commentPersistence: CommentPersistence,
    private votePersistence: VotePersistence,
    private subscriptionPersistence: SubscriptionPersistence
   ) { }

  /**
   * Get all admin users.
   */
  public getAdminUsers(): Promise<User[]> {
    return this.userPersistence.getAdminUsers();
  }

  /**
   * Delete user.
   *
   * @param userIdToDelete id of user to delete.
   * @param user context user.
   */
  public deleteUser(userIdToDelete: number, user: User): Promise<User> {
    // tslint:disable-next-line: no-bitwise
    if ((user.role_bits & Roles.Admin) === 0 || user.id !== userIdToDelete) {
      throw new Error('**ERR: Insufficient restoration permissions.');
    }
    return this.userPersistence.update(userIdToDelete, { disabled: true });
  }

  /**
   * Restore a deleted user.
   *
   * @param userIdToRestore is of user to restore.
   * @param user context user.
   */
  public restoreUser(userIdToRestore: number, user: User): Promise<User> {
    // tslint:disable-next-line: no-bitwise
    if ((user.role_bits & Roles.Admin) === 0) {
      throw new Error('**ERR: Insufficient restoration permissions.');
    }
    return this.userPersistence.update(userIdToRestore, { disabled: false });
  }

  /**
   * Get user from object.
   *
   * @param user user object from context.
   */
  public me(user: any): Promise<any> {
    if (!user) { throw new Error(AppErrors.NotAuthenticated); }
    // user is authenticated
    return this.userPersistence.findAllOfMeById(user.id);
  }

  /**
   * Take parameters and return matching users using the
   * persistence layer.
   *
   * @param firstName user first name.
   * @param lastName user last name.
   * @param email user email.
   */
  public findUsers(firstName: string, lastName: string, email: string): Promise<any[]> {
    return this.userPersistence.find(firstName, lastName, email);
  }

  /**
   * Find a user by their username and include models.
   *
   * @param username user username.
   * @param param1 user deconstructed from context.
   */
  public findFullUser(username, { user }): Promise<any> {
    if (username === user.username) {
      // Get the users own full values.
      return this.me(user);
    }
    return this.userPersistence.findUserByUsername(username);
  }

  /**
   * Update a user object in the database.
   *
   * @param user current user (just need it for the id).
   * @param newUserProps new properties to set.
   */
  public updateUser(user: any, newUserProps: User): Promise<any> {
    // Get possible handles we might need to update later.
    const { username, email } = newUserProps;
    // Update the user.
    return this.userPersistence.update(user.id, newUserProps)
      .then(updatedUser => {
        const userAuthHandlesToUpdate = [];
        if (email && email !== updatedUser.email) {
          // Update email UA.
          userAuthHandlesToUpdate.push(email);
        }
        if (username && username !== updatedUser.username) {
          // Update username UA
          userAuthHandlesToUpdate.push(username);
        }
        return Promise.all(userAuthHandlesToUpdate.map(handle => {
          if (handle) {
            return this.userAuthPersistence.findUserAuthByHandle(handle)
              .then(userAuth => this.userAuthPersistence.updateUserAuth(userAuth.id, { handle }));
          }
        }))
        .then(() => updatedUser);
      });
  }

  /**
   * Get user stats.
   *
   * @param userId id of user.
   */
  public getUserStats(username: string): Promise<UserStats> {
    return this.userPersistence.findByUsername(username)
    .then(user => (
      Promise.all([
        this.sitePersistence.getCountOfUserSites(user.id),
        this.commentPersistence.getCountOfUsersComments(user.id),
        this.votePersistence.getTotalAccumulatedVotes(user.id, false),
        this.votePersistence.getTotalAccumulatedVotes(user.id, true),
        this.subscriptionPersistence.getCountOfAllUserSubscribers(user.id),
        this.subscriptionPersistence.getCountOfAllUserSubscribedTo(user.id)
      ])
      .then((res) => ({
        total_sites_created: res[0],
        total_comments_made: res[1],
        total_site_downvotes: res[2],
        total_site_upvotes: res[3],
        total_subscribers: res[4],
        total_subscribed_to: res[5]
      }))
    ));
  }
}
