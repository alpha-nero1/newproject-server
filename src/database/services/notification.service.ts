/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { Notification, NotificationQueryOpts } from '../../models/types/notification/notification.interface';
import { NotificationPersistence } from '../persistence/notification.persistence';
import { User } from '../../models/types/user.interface';
import { GeneralQueryOpts } from '../../schema/types/general-query-opts.interface';

/**
 * @author Alessandro Alberga
 * @description Service class managing user activities, none of the creation code should
 *   be exposed as an API.
 */
export class NotificationService {
  constructor(
    private notificationPersistence: NotificationPersistence
  ) {

  }

  /**
   * Create a user activity record.
   *
   * @param activity activity record to create.
   * @param opts options to insert into create.
   */
  public create(activity: Notification, user: User, sequelizeOpts?: any): Promise<Notification> {
    // Ensure that the activity correlates ALWAYS to the context user.
    const activityToSave = {
      ...activity,
      user_id: user.id
    };
    return this.notificationPersistence.create(activityToSave, sequelizeOpts);
  }

  /**
   * Get user activities.
   *
   * @param generalOpts general query options.
   * @param user context user of request.
   * @param sequelizeOpts sequelize options to include. e.g transaction.
   */
  public getNotifications(
    generalOpts: GeneralQueryOpts,
    user: User,
    sequelizeOpts?: any
  ): Promise<{ count: number, rows: Notification[] }> {
    const queryOpts: NotificationQueryOpts = {
      ...generalOpts,
      user_id: user.id
    };
    return this.notificationPersistence.getNotifications(queryOpts, sequelizeOpts);
  }

  /**
   * Read notifications.
   *
   * @param notificationIds notification ids.
   * @param contextUser context user.
   */
  public readNotifications(notificationIds: number[], contextUser: User): Notification[] {
    return this.notificationPersistence.readNotifications(notificationIds, contextUser);
  }
}
