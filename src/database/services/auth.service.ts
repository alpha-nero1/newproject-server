/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import bcrypt from 'bcrypt';
import { UserPersistence } from '../persistence/user.persistence.js';
import { UserAuthPersistence } from '../persistence/user-auth.persistence.js';
import { User } from '../../models/types/user.interface.js';
import validator from 'validator';
import Utils from '../../common/utils.js';
import { SesManager } from '../third-party/ses-manager.service.js';
import { EmailTemplate } from '../../models/types/email-template.interface.js';
import { UserAuth } from '../../models/types/user-auth.interface.js';
import { RolePersistence } from '../persistence/role.persistence.js';

/**
 * @author Alessandro Alberga
 * @description handles in house authentication.
 */
export class AuthService {

  constructor(
    private userPersistence: UserPersistence,
    private userAuthPersistence: UserAuthPersistence,
    private sesManager: SesManager,
    private rolePersistence: RolePersistence
  ) { }

  /**
   * Verify that user has access to operations.
   *
   * @param user context user.
   * @param operations operations list.
   */
  public verifyOperationsAccess(user: User, operations: string[]): Promise<boolean> {
    return this.rolePersistence.verifyOperationsAccess(user, operations);
  }

  /**
   * Authenticate a user using a handle and password.
   *
   * @param handle handle user used to authenticate.
   * @param password password to authenticate user auth with.
   */
  public authenticate(handle: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userAuthPersistence.findUserAuthByHandle(handle)
        .then(userAuth => {
          // If no user auth found, reject.
          if (!userAuth) {
            // Resolve nothing. login failed.
            return resolve({
              token: null,
              expiry_date: null
            });
         }
          // Compare password of userAuth and password sent in.
          return bcrypt.compare(password, userAuth.password)
            .then((authenticated: boolean) => {
            // Resolve nothing. login failed.
            if (!authenticated) {
                resolve({
                  token: null,
                  expiry_date: null
                });
              } else {
                // User is authenticated, send back a token.
                this.userAuthPersistence.findMyValidUserAuthToken(userAuth.id)
                  .then(userAuthToken => {
                    if (userAuthToken) {
                      // Return the user auth token if valid one found, update expiry first.
                      this.userAuthPersistence.updateTokenExpiry(userAuthToken.token)
                        .then(() => resolve({
                          token: userAuthToken.token,
                          expiry_date: userAuthToken.expiry_date
                        }));
                    } else {
                      // Else create the user auth token before sending.
                      this.userAuthPersistence.createUserAuthToken(userAuth.id)
                        .then(createdUserAuthToken => {
                          resolve({
                            token: createdUserAuthToken.token,
                            expiry_date: createdUserAuthToken.expiry_date
                          });
                        });
                    }
                  });
              }
            });
        });
    });
  }

  /**
   * Register a user.
   *
   * @param user user to register.
   */
  public register(user: any): Promise<any> {
    const { username, email, password } = user;
    // Get rid of password.
    delete user.password;
    return new Promise((resolve, reject) => {
      // Check for existing users.
      this.userAuthPersistence.findUserAuthsByHandles([username, email])
        .then(userAuths => (
          userAuths.map(ua => {
            const reason = validator.isEmail(ua.handle) ?
              `User with email ${ua.handle} already exists.` :
              `user with username ${ua.handle} already exists.`;
            reject({ success: false, reason });
          })
        ))
        .then(() => {
          // Checks passed, create user!
          this.userPersistence.create(user)
            .then((userCreated: User) => {
              // Encrypt password to put in database.
              Utils.encrypt(password)
                .then(passwordToSave => (
                  // Make user auth for each handle provided
                  Promise.all([username, email].map(handle => (
                    this.userAuthPersistence.createUserAuth({
                      handle,
                      user_id: userCreated.id,
                      password: passwordToSave
                    })
                  )))
                ))
                .then(() => resolve({ success: true }));
            })
            .catch(() => reject({ success: false }));
        });
    });
  }

  /**
   * Create the user auth reset and send notification email.
   */
  public initiatePasswordReset(handle: string, contextUser: User): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userAuthPersistence.findUserAuthByHandle(handle)
        .then(userAuth => {
          if (!userAuth) {
            reject(null);
          } else {
            // Create user auth reset.
            this.userAuthPersistence.createUserAuthReset(userAuth.id)
              .then(userAuthReset => {
                // Create email template.
                const emailTemplate: EmailTemplate = {
                  template_name: 'PasswordReset',
                  template_data: {
                    title: 'Reset your password',
                    reset_text: 'Follow the url to update your password. The link will expire in 1 day.',
                    reset_token: userAuthReset.reset_token
                  }
                };
                // Send support email.
                this.sesManager.sendSupportEmail([userAuth.handle], 'Password Reset', emailTemplate)
                  .then(res => resolve(JSON.stringify(res)));
              });
          }
        });
    });
  }

  /**
   * Reset the user auth password.
   *
   * @param resetToken the token to verify reset.
   * @param newPassword new password to set for UA.
   */
  public finalisePasswordReset(resetToken: string, newPassword: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.userAuthPersistence.findUserAuthReset(resetToken)
        .then(tokenRecord => {
          if (!tokenRecord) {
            reject(null);
          }
          // Find the user auth connected to this reset.
          this.userAuthPersistence.findUserAuthById(tokenRecord.user_auth_id)
            .then((userAuth: UserAuth) => {
              // Modify UA and update.
              Utils.encrypt(newPassword)
                .then(encryptedPassword => {
                  userAuth.password = encryptedPassword;
                  userAuth.updated_at = new Date();
                  resolve(this.userAuthPersistence.updateUserAuth(userAuth.id, userAuth));
                })
                .catch(() => reject(null));
            })
            .catch(() => reject(null));
        });
    });
  }

  /**
   * Take the context and return a decoded user object.
   *
   * @param context context object.
   */
  public getUserFromUserAuthToken(token: string): Promise<any> {
    return this.userPersistence.findUserByUserAuthToken(token);
  }

  /**
   * Update token expiry.
   *
   * @param token token to update.
   */
  public updateTokenExpiry(token: string): Promise<any> {
    return this.userAuthPersistence.updateTokenExpiry(token);
  }
}
