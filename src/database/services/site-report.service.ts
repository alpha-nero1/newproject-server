/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { SiteReportPersistence } from '../persistence/site-report.persistence';
import { SitePersistence } from '../persistence/site.persistence';
import { User } from '../../models/types/user.interface';
import { SiteReport } from '../../models/types/site-report.interface';
import { Site } from '../../models/types/site.interface';
import { ListReportedSitesParams } from '../../schema/types/list-reported-sites-params.interface';
import { AuthService } from './auth.service';

/**
 * @author Alessandro Alberga
 * @description Site report service.
 */
export class SiteReportService {
  constructor(
    private siteReportPersistence: SiteReportPersistence,
    private sitePersistence: SitePersistence,
    private authService: AuthService
  ) { }

  /**
   * Query for site reports.
   *
   * @param opts ptions to query site reports by.
   */
  public findAllByOptions(opts: ListReportedSitesParams) {
    return this.siteReportPersistence.findAllByOptions(opts);
  }

  /**
   * Report a site.
   *
   * @param siteId id of site to report.
   * @param user context user object.
   */
  public reportSite(siteId: number, reason: string, user: User): Promise<any> {
    return this.siteReportPersistence.reportSite(siteId, reason, user.id)
      .then(res => {
        // Update if the site has now passed the threshold.
        return this.updateSiteThreshold(siteId);
      });
  }

  /**
   * Dismiss a list of site reports.
   *
   * @param siteReportIds ids of reports to dismiss.
   */
  public dismissSiteReports(siteReportIds: number[]): Promise<Site[]> {
    return this.siteReportPersistence.dismissReports(siteReportIds)
      .then((reports) => {
        const siteIdsToCheck = reports?.map(rep => rep.site_id);
        // Update if the site is now below the threshold.
        return Promise.all(siteIdsToCheck?.map(id => this.updateSiteThreshold(id)));
      });
  }

  /**
   * Will update the passed report threshold of a site depending on if the
   * threshold is actually met or not.
   *
   * @param siteId id of the site to evaluate.
   */
  private updateSiteThreshold(siteId: number): Promise<Site> {
    // Find all site reports on the site by using options.
    return this.siteReportPersistence.findAllByOptions({ site_id: siteId })
      .then((response: SiteReport[]) => {
        // If we found five or more reports, update the threshold, it is no longer
        // publically available.
        if (response.length >= 5) {
          // Shoot off the site update.
          return this.sitePersistence.update(siteId, {
            passed_report_threshold: true
          });
        }
        // Else lets ensure that the site is still okay to see.
        return this.sitePersistence.update(siteId, {
          passed_report_threshold: false
        });
      });
  }

  /**
   * Get reported sites.
   *
   * @param params options for getting reported sites.
   */
  public getReportedSitesByParams(params: ListReportedSitesParams): Promise<Site[]> {
    return this.siteReportPersistence.getReportedSitesByParams(params);
  }
}
