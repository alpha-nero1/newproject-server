/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { CommentPersistence } from '../persistence/comment.persistence';
import { VotePersistence } from '../persistence/vote.persistence';
import { NotificationPersistence } from '../persistence/notification.persistence';
import { SitePersistence } from '../persistence/site.persistence';
import { User } from '../../models/types/user.interface';
import Utils from '../../common/utils';
import { Comment } from '../../models/types/comment.interface';
import { NotificationMessages } from '../../models/types/notification/notification-messages';
import { GeneralQueryOpts } from '../../schema/types/general-query-opts.interface';
import { RowsAndCount } from '../../models/types/general/rows-and-count.interface';

/**
 * @author Alessandro Alberga
 * @description handles commenting on all entities.
 */
export class CommentService {
  constructor(
    private commentPersistence: CommentPersistence,
    private votePersistence: VotePersistence,
    private notificationPersistence: NotificationPersistence,
    private sitePersistence: SitePersistence
  ) { }

  /**
   * Simply find the comment record by id.
   *
   * @param id id of comment.
   */
  public findByPk(id: number): Promise<Comment> {
    return this.commentPersistence.findByPk(id);
  }

  /**
   * Use persistence tp get sites.
   *
   * @param siteId site id.
   */
  public findCommentsBySiteId(siteId: number, userId: number, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Comment>> {
    return this.commentPersistence.findCommentsBySite(siteId, userId, queryOpts)
    .then(res => (
      Promise.all(res.rows.map(comment => this.commentPersistence.findRepliesByCommentId(comment.id, userId, queryOpts)))
      .then((commentReplies) => {
        const response = res;
        response.rows = response.rows.map((comment, i) => ({
          ...comment.dataValues,
          ...comment,
          replies: commentReplies[i]
        }));
        return response;
      })
    ));
  }

  /**
   * Takes comment ids, generally that of a page to refresh and gets new values from the DB.
   *
   * @param ids ids to refresh.
   * @param userId contextual user id.
   * @param queryOpts query options, sort value here is important.
   */
  public findCommentsByIds(ids: number[], user: User, queryOpts: GeneralQueryOpts): Promise<Comment[]> {
    return this.commentPersistence.findCommentsByIds(ids, user.id, queryOpts);
  }

  /**
   * Get a paginated list of replies under a comment.
   *
   * @param commentId parent comment id.
   * @param queryOpts general query options.
   */
  public findRepliesByCommentId(commentId: number, user: User, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Comment>> {
    return this.commentPersistence.findRepliesByCommentId(commentId, user.id, queryOpts);
  }

  /**
   * Comment on a site. Return full list of comments.
   *
   * @param comment comment object.
   * @param user context user.
   */
  public commentOnSite(siteId: number, comment: any, user: User): Promise<Comment> {
    const commentToCreate = {
      ...comment,
      user_id: user.id
    };
    // Else, execute the comment on site.
    return this.commentPersistence.transaction(transaction => (
      this.sitePersistence.getSiteById(siteId, transaction)
      .then(foundSite => (
        this.commentPersistence.comment(commentToCreate, transaction)
        .then(createdComment => (
          foundSite.addComment(createdComment, { transaction })
            .then((res) => (
              this.notificationPersistence.create(
                {
                  user_id: foundSite?.user_id,
                  title:  NotificationMessages.SomeoneCommentedOnYourSite({ user, site: foundSite, comment: createdComment }),
                  slug: `goto-site-${foundSite?.uuid}`
                },
                { transaction }
              )
            ))
        ))
    ))
    ));
  }

  /**
   * Reply to a comment. return full list of comments.
   *
   * @param reply reply object. CONTAINS parent_comment_id
   * @param user context user.
   */
  public replyToComment(parentCommentId: number, reply: Comment, user: User): Promise<Comment> {
    const replyToCreate = {
      ...reply,
      parent_comment_id: parentCommentId,
      user_id: user.id
    };
    return this.sitePersistence.transaction(transaction => (
      this.commentPersistence.comment(replyToCreate, transaction)
        .then(createdReply => (
          this.commentPersistence.findCommentById(parentCommentId)
            .then(commentRepliedTo => (
              this.notificationPersistence.create(
                {
                  user_id: commentRepliedTo.user_id,
                  title: NotificationMessages.SomeoneRepliedToYourComment({ user, comment: commentRepliedTo, reply: createdReply }),
                  slug: `goto-site-${commentRepliedTo?.site?.uuid}`
                },
                { transaction }
              )
              .then(() => commentRepliedTo)
            ))
        )
    )));
  }

  /**
   * Delete comment.
   *
   * @param commentId id of comment
   */
  public deleteComment(commentId: number): Promise<any> {
    return this.commentPersistence.deleteCommentById(commentId);
  }
}
