/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { RolePersistence } from '../persistence/role.persistence';
import { User } from '../../models/types/user.interface';

/**
 * Enum representing all posible roles.
 */
export enum Roles {
  Normal = 0,
  Admin = 1,
  SuperAdmin = 2,
  Emperor = 4
}

/**
 * @author Alessandro Alberga
 * @description Role service.
 */
export class RoleService {
  constructor(private rolePersistence: RolePersistence) { }

  /**
   * Getter for all roles in system.
   */
  public getRoles(): Promise<any[]> {
    return this.rolePersistence.getAllRoles();
  }

  /**
   * Allow for assignment of roles.
   */
  public assignRolesToUser(userIdToAssign: number, roleBits: number, user: User) {
    // tslint:disable-next-line: no-bitwise
    const isSuperAdmin = ((user.role_bits & Roles.SuperAdmin) !== 0);
    if (!isSuperAdmin || (roleBits > 2)) {
      throw new Error('**ERR: Insufficient role assigning permissions');
    }
    return this.rolePersistence.assignRolesToUser(userIdToAssign, roleBits);
  }
}
