/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import errors from '../../common/appErrors';
import { NotificationPersistence } from '../persistence/notification.persistence';
import { VotePersistence } from '../persistence/vote.persistence';
import { CommentPersistence } from '../persistence/comment.persistence';
import { User } from '../../models/types/user.interface';
import { SitePersistence } from '../persistence/site.persistence';
import Utils from '../../common/utils';

/**
 * @author Alessandro Alberga
 * @description service level class for voting functionality.
 */
export class VoteService {

  constructor(
    private votePersistence: VotePersistence,
    private notificationPersistence: NotificationPersistence,
    private commentPeristence: CommentPersistence,
    private sitePersistence: SitePersistence
    ) { }

  /**
   * Vote on a site.
   *
   * @param siteId site to vote on.
   * @param user_id id of user performing vote.
   * @param up 0/1 to upvote or downvote.
   */
  public async voteOnSite(user: any, siteId: number, up: boolean): Promise<any> {
    if (!user || !user.id) {
      throw new Error(errors.UnauthorisedError);
    }
    const actingUserName = [user.first_name, user.last_name].join(' ');
    return this.votePersistence.transaction(transaction => (
      this.votePersistence.voteOnSite(siteId, user.id, up, transaction)
      .then(() => (
        this.sitePersistence.getSiteWithVotes(siteId, transaction)
        .then(site => (
          // Create activity record of what just happened!
          this.notificationPersistence.create({
            user_id: site?.user_id,
            title: `${actingUserName} (${user.username}) ${up ? 'up' : 'down' }-voted on your site **${site?.title}**`,
            slug: `goto-site-${site?.uuid}`
          }, { transaction })
          .then(() => site)
        ))
      ))
    ));
  }

  /**
   * Vote on a comment and return votes.
   *
   * @param commentId id of site comment.
   * @param user_id id of acting user.
   * @param up 1/0 of down or up vote.
   */
  public async voteOnComment(user: User, commentId: number, up: boolean): Promise<any> {
    if (!user || !user.id) {
      throw new Error(errors.UnauthorisedError);
    }
    const actingUserName = [user.first_name, user.last_name].join(' ');
    // Initiate a transaction.
    return this.votePersistence.transaction(transaction => (
      this.votePersistence.voteOnComment(commentId, user.id, up, transaction)
      .then(() => (
        this.commentPeristence.findCommentById(commentId, transaction)
        .then(comment => (
          // Create activity record of what just happened!
          this.notificationPersistence.create({
            user_id: comment.user_id,
            title: `${actingUserName} (${user.username}) ${up ? 'up' : 'down' }-voted on your comment **${Utils.ElipsifyText(comment.text, 20)}**`,
            slug: `goto-site-${comment?.site?.uuid}`
          }, { transaction })
          .then(() => this.votePersistence.getCommentVotes(commentId))
        ))
      ))
    ));
  }
}
