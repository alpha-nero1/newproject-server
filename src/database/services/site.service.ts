/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import uuid from 'uuid/v4';
import { SitePersistence } from '../persistence/site.persistence';
import { UserPersistence } from '../persistence/user.persistence';
import { DailyFeature } from '../../models/types/daily-feature.interface';
import { NotificationPersistence } from '../persistence/notification.persistence';
import { Site } from '../../models/types/site.interface';
import moment from 'moment';
import { CommentPersistence } from '../persistence/comment.persistence';
import { Constants } from '../../common/constants';
import { VotePersistence } from '../persistence/vote.persistence';
import { RowsAndCount } from '../../models/types/general/rows-and-count.interface';
import { GeneralQueryOpts } from '../../schema/types/general-query-opts.interface';

/**
 * @author Alessandro Alberga
 * @description service layer class for sites.
 */
export class SiteService {
  constructor(
    private sitePersistence: SitePersistence,
    private userPersistence: UserPersistence,
    private notificationPersistence: NotificationPersistence,
    private commentPersistence: CommentPersistence,
    private votePersistence: VotePersistence
  ) { }

  /**
   * Simply get a site by its id.
   *
   * @param id id of site.
   */
  public getByPk(id: number): Promise<Site> {
    return this.sitePersistence.getSiteById(id);
  }

  /**
   * List the trending sites.
   */
  public listTrendingSites(opts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    return this.sitePersistence.getTrendingSites(opts);
  }

  /**
   * Get sites that the context user has upvotes on.
   *
   * @param user context user.
   * @param page page of results.
   */
  public getSitesWithVotes(user: any, up: boolean, page = 0): Promise<any[]> {
    return this.sitePersistence.getSitesWithVotes(user.id, up, page);
  }

  /**
   * List recommended sites for a user.
   *
   * @param user user to evaluate.
   */
  public async listRecommendedSitesByUser(user: any, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    // Find the full user from context, considering we need tags.
    const fullUser = await this.userPersistence.findOneById(user.id);
    return this.sitePersistence.findRecommendedSites(fullUser, queryOpts);
  }

  /**
   * Create a site.
   *
   * @param site site to create.
   */
  public createSite({ user }, site: any): Promise<Site> {
    if (!user || !user.id) {
      throw new Error('**FATAL: Unrecognised entitiy.');
    }
    // Insert user id into the site object.
    const siteToSave = {
      ...site,
      user_id: user.id,
      uuid: uuid()
    };
    return this.sitePersistence.transaction(transaction => (
      this.sitePersistence.create(siteToSave, transaction)
      .then(createdSite => (
        // Add this to the users activity feed.
        this.notificationPersistence.create({
          user_id: user.id,
          title: `You created a new site ${createdSite.title}`,
          slug: `goto-site-${createdSite.uuid}`
        }, { transaction })
        // Notify subscribers!
        .then(() => createdSite)
      ))
    ));
  }

  /**
   * Update a site.
   *
   * @param id id of site to mutate.
   * @param newValues new values of the site.
   * @param contextUser context user.
   */
  public updateSite(id: number, newValues: any, contextUser: any): Promise<Site> {
    if (!contextUser || !contextUser.id) {
      throw new Error('**FATAL: Unrecognised entitiy.');
    }
    return this.sitePersistence.transaction(transaction => (
      this.sitePersistence.update(id, newValues)
      .then(updatedSite => (
        // Add this to the users activity feed.
        this.notificationPersistence.create({
          user_id: contextUser.id,
          title: `You made some edits to ${updatedSite.title}`,
          slug: `goto-site-${updatedSite.uuid}`
        }, { transaction })
        .then(() => updatedSite)
      ))
    ));
  }

  /**
   * Retrieve a site using its corresponding uuid.
   *
   * @param uuid unique id of site.
   */
  public getSiteByUuid(siteUuid: string, { user }): Promise<Site> {
    // Get the site.
    return this.sitePersistence.getSiteByUuid(siteUuid, user)
    .then(site => {
      const retSite = site;
      if (site) {
        // Get the votes.
        return this.votePersistence.getSiteVotes(site.id)
        .then(({ upvotes, downvotes }) => {
          retSite.upvotes = upvotes;
          retSite.downvotes = downvotes;
          // Get the comments.
          return this.commentPersistence.findCommentsBySite(site.id, user.id, { page: 0, page_size: Constants.initialCommentLimit })
          .then((comments) => {
            retSite.comments = comments;
            return retSite;
          });
        });
      }
      return retSite;
    });
  }

  /**
   * Search sites.
   *
   * @param keyword word to search for.
   * @param tags site tags to filter by.
   * @param queryOpts query opts.
   * @param transaction optional transaction.
   */
  public searchSites(
    keyword: string,
    tags: string[],
    queryOpts: GeneralQueryOpts,
    transaction?: any
  ): Promise<RowsAndCount<Site>> {
    return this.sitePersistence.searchSites(
      keyword,
      tags,
      queryOpts,
      transaction
    );
  }

  /**
   * Get users sites.
   *
   * @param username id of user.
   * @param queryOpts query options.
   */
  public getUsersSites(username: string, queryOpts: GeneralQueryOpts): Promise<RowsAndCount<Site>> {
    return this.userPersistence.findUserByUsername(username)
      .then(user => (
        this.sitePersistence.getUsersSites(user.id, queryOpts)
      ));
  }

  /**
   * List the daily features.
   *
   * @param opts options.
   */
  public getDailyFeatures(opts: any) {
    return this.sitePersistence.getDailyFeatures(opts);
  }

  /**
   * Create a daily feature.
   *
   * @param feature daily feature.
   */
  public createDailyFeature(feature: DailyFeature) {
    const featureDate = moment(new Date()).format('LL');
    return this.sitePersistence.transaction(transaction => (
      this.sitePersistence.createDailyFeature(feature)
      .then(createdFeature => (
        this.sitePersistence.getSiteById(createdFeature.site_id, transaction)
        .then(site => (
          this.notificationPersistence.create({
            user_id: site.user_id,
            title: `Your site **${site.title}** was made the feature of ${featureDate}, congratulations!`,
            slug: `goto-site-${site.uuid}`
          }, { transaction })
        ))
        .then(() => createdFeature)
      ))
    ));
  }
}
