/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { NotificationPersistence } from '../persistence/notification.persistence';

/**
 * @author Alessandro Alberga
 * @description service layer class for subscription operations.
 */
export class SubscriptionService {

  constructor(
    private subscriptionPersistence,
    private notificationPersistence: NotificationPersistence
  ) { }

  /**
   * Simple interfacing function to create a subscription.
   *
   * @param userId id of the acting user.
   * @param subscribeeId id of the user to subscribe to.
   */
  public async subscribe(userId: number, subscribeeId: number): Promise<any> {
    const subscription = { subscriber_user_id: userId, subscribee_user_id: subscribeeId };
    const alreadySubscribed = await this.getMySubscribees(userId);

    // Make sure user cannot subscribe to themselves.
    if (!alreadySubscribed.count && userId !== subscribeeId) {
      return this.subscriptionPersistence.create(subscription);
    }
    return alreadySubscribed;
  }

  /**
   * Simple interface function to unsubscribe to a subscription.
   *
   * @param userId id of the acting user.
   * @param subscribeeId id of the user to unsubscribe to.
   */
  public unsubscribe(userId: number, subscribeeId: number): Promise<any> {
    return this.subscriptionPersistence.delete(userId, subscribeeId);
  }

  /**
   * Get the users who i have subscribed TO.
   *
   * @param userId user id.
   */
  public getMySubscribers(userId: number): Promise<any> {
    return this.subscriptionPersistence.getMySubscribers(userId);
  }

  /**
   * Get the users who are subscribed to ME.
   *
   * @param userId user id.
   */
  public getMySubscribees(userId: number): Promise<any> {
    return this.subscriptionPersistence.getMySubscribees(userId);
  }
}
