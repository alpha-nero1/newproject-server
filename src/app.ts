/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import * as restify from 'restify';
import graphqlHTTP from 'express-graphql';
import corsMiddleware from 'restify-cors-middleware';
import dependencies from './database/dependecy_injection/dependency_injection';
import config from './config/config.json';
import { GraphqlHandler } from './schema/graphqlHandler';

// Use restify to instantiate our app.
const app = restify.createServer();
const api = dependencies.container.api;

// Implement CORS middleware.
const cors = corsMiddleware({
  origins: ['*'],
  allowHeaders: ['Authorization'],
  exposeHeaders: ['Authorization']
});

app.pre(cors.preflight);
app.use(cors.actual);

// Implement query and body parser.
app.use(restify.plugins.bodyParser());
app.use(restify.plugins.queryParser());

// Setup the single endpoint.
app.post('/graphql', graphqlHTTP(GraphqlHandler(api)));

// Listen and run server...
app.listen({ port: config.server.port }, () =>
  console.log(
    `Server ready at http://localhost:${config.server.port}/graphql ✅`
  )
);
