/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import uuid from 'uuid/v4';

export default {
  up: (queryInterface, Sequelize) =>
    Promise.resolve()
      .then(() =>
        queryInterface.createTable('users', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          first_name: {
            type: Sequelize.STRING
          },
          last_name: {
            type: Sequelize.STRING
          },
          username: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
          },
          email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
          },
          password: {
            type: Sequelize.STRING
          },
          date_of_birth: {
            type: Sequelize.DATE
          },
          tags: {
            type: Sequelize.ARRAY(Sequelize.STRING)
          },
          auth_token: {
            type: Sequelize.TEXT
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        })
      )
      .then(() =>
        queryInterface.createTable('sites', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          uuid: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: uuid()
          },
          user_id: {
            type: Sequelize.INTEGER,
          },
          title: {
            allowNull: false,
            type: Sequelize.STRING
          },
          content: {
            allowNull: false,
            type: Sequelize.TEXT
          },
          tags: {
            type: Sequelize.ARRAY(Sequelize.STRING)
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        })
      ),
  down: queryInterface =>
    Promise.resolve()
      .then(() => queryInterface.dropTable('users'))
      .then(() => queryInterface.dropTable('sites'))
};
