/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { QueryInterface } from 'sequelize/types';
import uuid from 'uuid/v4';

export default {
  up: (queryInterface: QueryInterface, Sequelize: any) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
        .then(() => queryInterface.addColumn('folders', 'uuid', {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.UUID,
            defaultValue: uuid()
        }, { transaction }))
    ))
  )
}