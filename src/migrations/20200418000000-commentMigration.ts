
export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      queryInterface.dropTable('reply_upvotes', { transaction })
      .then(() => queryInterface.dropTable('reply_downvotes', { transaction }))
      .then(() => queryInterface.dropTable('comment_replies', { transaction }))
      .then(() => queryInterface.dropTable('comment_upvotes', { transaction }))
      .then(() => queryInterface.dropTable('comment_downvotes', { transaction }))
      .then(() => queryInterface.dropTable('site_comments', { transaction }))
      // Standalone comment table, has parent.
      .then(() => queryInterface.createTable('comments', {
        id: {
          allowNull: false,
          primaryKey: true,
          unique: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        parent_comment_id: {
          references: {
            model: 'comments',
            key: 'id'
          },
          type: Sequelize.INTEGER
        },
        text: {
          type: Sequelize.TEXT
        },
        user_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'users',
            key: 'id'
          },
          allowNull: false
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction }))
      // Comment upvotes
      .then(() => queryInterface.createTable('comment_upvotes', {
        id: {
          allowNull: false,
          primaryKey: true,
          unique: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        comment_id: {
          references: {
            model: 'comments',
            key: 'id'
          },
          allowNull: false,
          type: Sequelize.INTEGER
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction }))
      // Comment downvotes
      .then(() => queryInterface.createTable('comment_downvotes', {
        id: {
          allowNull: false,
          primaryKey: true,
          unique: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        comment_id: {
          references: {
            model: 'comments',
            key: 'id'
          },
          allowNull: false,
          type: Sequelize.INTEGER
        },
        user_id: {
          references: {
            model: 'users',
            key: 'id'
          },
          allowNull: false,
          type: Sequelize.INTEGER
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction }))
      // Link table between sites and comments
      .then(() => queryInterface.createTable('site_comments', {
        id: {
          allowNull: false,
          primaryKey: true,
          unique: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        comment_id: {
          references: {
            model: 'comments',
            key: 'id'
          },
          allowNull: false,
          type: Sequelize.INTEGER
        },
        site_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'users',
            key: 'id'
          },
          allowNull: false,
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction }))
      .then(() => queryInterface.renameTable('user_activities', 'notifications', { transaction }))
    ))
  ),
  down: () => (
    Promise.resolve()
  )
};
