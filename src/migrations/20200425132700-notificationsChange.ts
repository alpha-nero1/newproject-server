export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.addColumn('notifications', 'subscribee_id', {
        references: {
          model: 'users',
          key: 'id'
        },
        type: Sequelize.INTEGER
      }, { transaction }))
    ))
  )
};
