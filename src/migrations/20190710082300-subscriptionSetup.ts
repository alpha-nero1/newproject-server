/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
        .then(() => queryInterface.createTable('subscriptions', {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
          },
          // User who made the subscription. (Subscriber)
          subscriber_user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          // User who is being subscribed to. (Subscribee)
          subscribee_user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        .then(() => queryInterface.createTable('notifications', {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
          },
          user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          notification_text: {
            type: Sequelize.STRING
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
    ))
  ),
  down: () => Promise.resolve()
}