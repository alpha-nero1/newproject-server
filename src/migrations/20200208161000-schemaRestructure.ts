/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2020: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { QueryInterface } from 'sequelize/types';

export default {
  up: (queryInterface: QueryInterface, Sequelize: any) => (
    queryInterface.sequelize.transaction(transaction => (
      // First drop folder related stuff, we are scrapping this functionality.
      queryInterface.dropTable('folder_sites', { transaction })
        .then(() => queryInterface.dropTable('folders', { transaction }))
        // Create the site reports table.
        .then(() => queryInterface.createTable('site_reports', {
            id: {
              allowNull: false,
              primaryKey: true,
              autoIncrement: true,
              type: Sequelize.INTEGER
            },
            site_id: {
              type: Sequelize.INTEGER,
              allowNull: false,
              references: {
                model: 'sites',
                key: 'id'
              }
            },
            reporting_user_id: {
              type: Sequelize.INTEGER,
              allowNull: false,
              references: {
                model: 'users',
                key: 'id'
              }
            },
            dismissed: {
              allowNull: false,
              type: Sequelize.BOOLEAN,
              defaultValue: false
            },
            disabled: {
              allowNull: false,
              type: Sequelize.BOOLEAN,
              defaultValue: false
            },
            created_at: {
              allowNull: false,
              defaultValue: Sequelize.NOW,
              type: Sequelize.DATE
            },
            updated_at: {
              allowNull: false,
              defaultValue: Sequelize.NOW,
              type: Sequelize.DATE
            }
        }, { transaction }))
        .then(() => queryInterface.addColumn('users', 'role_bits', {
          allowNull: false,
          defaultValue: 0,
          type: Sequelize.INTEGER
        }, { transaction}))
        .then(() => queryInterface.addColumn('sites', 'passed_report_threshold', {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }, { transaction }))
        // Implement security around what operations can be accessed.
        .then(() => queryInterface.createTable('operations', {
          id: {
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          operation: {
            allowNull: false,
            type: Sequelize.STRING
          },
          role_bits: {
            allowNull: false,
            type: Sequelize.INTEGER
          },
          disabled: {
            allowNull: false,
            type: Sequelize.BOOLEAN,
            defaultValue: false
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.NOW,
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.NOW,
            type: Sequelize.DATE
          }
        }, { transaction }))
    ))
  )
}