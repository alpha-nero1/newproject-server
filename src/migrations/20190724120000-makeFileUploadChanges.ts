/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

export default {
  up: (queryInterface) => queryInterface.sequelize.transaction(transaction => (
    Promise.resolve()
      .then(() => queryInterface.sequelize.query(`
      ALTER TABLE users ADD COLUMN profile_photo VARCHAR(255);
    `, { transaction }))
  ))
}