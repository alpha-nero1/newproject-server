export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.createTable('daily_features', {
        id: {
          allowNull: false,
          primaryKey: true,
          unique: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        title: {
          allowNull: false,
          type: Sequelize.STRING
        },
        button_text: {
          allowNull: false,
          type: Sequelize.STRING
        },
        site_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'sites',
            key: 'id'
          }
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction }))
    ))
  ),
  down: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.dropTable('daily_features'))
    ))
  )
};
