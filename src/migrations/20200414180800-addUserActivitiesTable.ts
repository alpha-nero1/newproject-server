/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

/**
 * Add the user activities tracking to the new project.
 */
export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.createTable('user_activities', {
        id: {
          allowNull: false,
          primaryKey: true,
          unique: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        title: {
          allowNull: false,
          type: Sequelize.STRING
        },
        icon: {
          type: Sequelize.STRING
        },
        slug: {
          allowNull: true,
          type: Sequelize.STRING
        },
        read_time: {
          type: Sequelize.DATE
        },
        user_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'users',
            key: 'id'
          }
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction }))
    ))
  ),
  down: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.dropTable('user_activities'))
    ))
  )
};
