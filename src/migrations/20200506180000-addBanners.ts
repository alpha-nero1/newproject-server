export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.addColumn('users', 'banner_photo', {
        type: Sequelize.STRING
      }, { transaction }))
      .then(() => queryInterface.addColumn('sites', 'banner_photo', {
        type: Sequelize.STRING
      }, { transaction }))
      .then(() => queryInterface.renameColumn('subscriptions', 'user_id', 'subscriber_user_id', { transaction }))
      .then(() => queryInterface.renameColumn('subscriptions', 'subscribee_id', 'subscribee_user_id', { transaction }))
    ))
  ),
  down: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
      .then(() => queryInterface.removeColumn('users', 'banner_photo', { transaction }))
      .then(() => queryInterface.removeColumn('sites', 'banner_photo', { transaction }))
      .then(() => queryInterface.renameColumn('subscriptions', 'subscriber_user_id', 'user_id', { transaction }))
      .then(() => queryInterface.renameColumn('subscriptions', 'subscribee_user_id', 'subscribee_id', { transaction }))
    ))
  )
};