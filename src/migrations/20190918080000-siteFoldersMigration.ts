/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { QueryInterface, DataTypes, Transaction, Sequelize } from 'sequelize';

export default {
  up: (queryInterface: QueryInterface) => (
    queryInterface.sequelize.transaction((transaction: Transaction) => (
      Promise.resolve()
        .then(() => queryInterface.createTable('folders', {
          id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
          },
          parent_folder_id: {
            type: DataTypes.INTEGER,
            references: {
              model: 'folders',
              key: 'id'
            },
            allowNull: true
          },
          name: {
            type: DataTypes.STRING
          },
          user_id: {
            type: DataTypes.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            },
            allowNull: true
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: DataTypes.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: DataTypes.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: DataTypes.BOOLEAN
          }
        }, { transaction }))
        .then(() => queryInterface.createTable('folder_sites', {
          id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
          },
          folder_id: {
            type: DataTypes.INTEGER,
            references: {
              model: 'folders',
              key: 'id'
            },
            allowNull: true
          },
          site_id: {
            type: DataTypes.INTEGER,
            references: {
              model: 'sites',
              key: 'id'
            },
            allowNull: true
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: DataTypes.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: DataTypes.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: DataTypes.BOOLEAN
          }
        }, { transaction }))
    ))
  ),
  down: queryInterface => (
    queryInterface.sequelize.query(`
      DROP TABLE IF EXISTS folder_sites;
      DROP TABLE IF EXISTS folders;
    `)
  )
};
