/**
 * This source code is the confidential, proprietary information of
 * The New Project. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project.
 *
 * 2020: The New Project.
 * All Rights Reserved.
 */

import { QueryInterface } from 'sequelize/types';

// All opts to insert.
const operations = [
  { operation: 'listUsers', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'findFullUser', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date()},
  { operation: 'listRecommendedSitesByUser', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'listTrendingSites', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getSiteByUuid', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'findCommentsBySite', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getMySubscribees', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getMySubscribers', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'searchSites', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getTemporaryCredentials', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getSitesWithVotes', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'register', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'updateUser', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'authenticateUser', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'createSite', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'updateSite', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'addSiteVote', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'commentOnSite', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'replyToComment', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'deleteComment', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'deleteReply', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'voteOnSite', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'voteOnComment', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'voteOnReply', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'subscribe', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'unsubscribe', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'initiatePasswordRequest', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'finalisePasswordRequest', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'reportSite', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'dismissSiteReports', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getSitesWithVotes', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getReportSitesByParams', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getAllSiteReportsByParams', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'getAllRoles', role_bits: 2, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'assignRoleToUser', role_bits: 2, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'deleteUser', role_bits: 0, disabled: false, created_at: new Date(), updated_at: new Date() },
  { operation: 'restoreUser', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() }
];

const roles = [
  { role: 'admin', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() },
  { role: 'super_admin', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() },
  { role: 'emperor', role_bits: 1, disabled: false, created_at: new Date(), updated_at: new Date() },
];

export default {
  up: (queryInterface: QueryInterface, Sequelize: any) => (
    queryInterface.sequelize.transaction(transaction => (
      // Insert operations.
      queryInterface.bulkInsert('operations', operations, { transaction })
      // Create roles table so we have for later selections.
      .then(() => queryInterface.createTable('roles', {
        id: {
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
          type: Sequelize.INTEGER
        },
        role: {
          allowNull: false,
          type: Sequelize.STRING
        },
        role_bits: {
          allowNull: false,
          type: Sequelize.INTEGER
        },
        disabled: {
          allowNull: false,
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE
        }
      }, { transaction }))
      .then(() => queryInterface.bulkInsert('roles', roles, { transaction }))
    ))
  ),
  down: (queryInterface: QueryInterface, Sequelize: any) => (
    queryInterface.sequelize.transaction(transaction => (
      queryInterface.dropTable('roles', { transaction })
      .then(() => queryInterface.bulkDelete('operations', {}, { transaction }))
    )
    )
  )
};
