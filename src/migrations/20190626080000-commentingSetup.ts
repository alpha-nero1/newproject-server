/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

export default {
  up: (queryInterface, Sequelize) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
        // Setup comments table.
        .then(() => queryInterface.createTable('site_comments', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          text: {
            type: Sequelize.TEXT
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          site_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'sites',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        // Setup replies table.
        .then(() => queryInterface.createTable('comment_replies', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          text: {
            type: Sequelize.TEXT
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          site_comment_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'site_comments',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        .then(() => queryInterface.createTable('comment_downvotes', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          site_comment_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'site_comments',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        .then(() => queryInterface.createTable('comment_upvotes', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          site_comment_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'site_comments',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        // For replies
        .then(() => queryInterface.createTable('reply_downvotes', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          comment_reply_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'comment_replies',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        .then(() => queryInterface.createTable('reply_upvotes', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          comment_reply_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'comment_replies',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        // For sites
        .then(() => queryInterface.createTable('site_downvotes', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          site_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'sites',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
        .then(() => queryInterface.createTable('site_upvotes', {
          id: {
            allowNull: false,
            primaryKey: true,
            unique: true,
            autoIncrement: true,
            type: Sequelize.INTEGER
          },
          user_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          site_id: {
            type: Sequelize.INTEGER,
            references: {
              model: 'sites',
              key: 'id'
            }
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction }))
    ))
  ),
  down: () => true
}