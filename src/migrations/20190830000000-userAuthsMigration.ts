/**
 * This source code is the confidential, proprietary information of
 * The New Project Pty Ltd. here in, you may not disclose such Information,
 * and may only use it in accordance with the terms of the license
 * agreement you entered into with The New Project Pty Ltd.
 *
 * 2019: The New Project Pty Ltd.
 * All Rights Reserved.
 */

import { QueryInterface } from 'sequelize/types';
import uuid from 'uuid/v4';
import { CustomLogger } from '../common/customLogger';

const getUsers = (queryInterface, transaction) => (
  queryInterface.sequelize.query(`SELECT * FROM users`, { transaction })
    .then(users => users[0])
)

const makeUserAuthFromUser = (queryInterface, transaction, user) => (
  Promise.resolve()
    .then(() => (
      queryInterface.bulkInsert('user_auths', [
        {
          user_id: user.id,
          handle: user.username,
          password: (user.password ? user.password : 'Password1'),
          created_at: new Date(),
          updated_at: new Date(),
          disabled: false
        }
      ], { returning: true, transaction })
    ))
    .then(([userAuth]) => {
      CustomLogger.Log('user auth ->', userAuth);
      if (!userAuth.password) {
        CustomLogger.Warning(`user auth created has no password, user id: ${userAuth.user_id}`);
      }
      const expiryDate = new Date().setDate(new Date().getDay() + 7);

      return queryInterface.bulkInsert('user_auth_tokens', [{
        user_auth_id: userAuth.id,
        token: uuid(),
        expiry_date: new Date(expiryDate),
        created_at: new Date(),
        updated_at: new Date(),
        disabled: false
      }], { transaction })
    })
)

export default {
  up: (queryInterface: QueryInterface, Sequelize) => queryInterface.sequelize.transaction(transaction => (
    Promise.resolve()
      .then(() => (
        queryInterface.createTable('user_auths', {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
          },
          user_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'users',
              key: 'id'
            }
          },
          handle: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
          },
          password: {
            type: Sequelize.STRING,
            allowNull: false
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction })
      ))
      .then(() => (
        queryInterface.createTable('user_auth_resets', {
          id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
          },
          user_auth_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'user_auths',
              key: 'id'
            }
          },
          reset_token: {
            allowNull: false,
            type: Sequelize.STRING
          },
          expiry_date: {
            allowNull: false,
            type: Sequelize.DATE
          },
          created_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          updated_at: {
            allowNull: false,
            defaultValue: Sequelize.literal('NOW()'),
            type: Sequelize.DATE
          },
          disabled: {
            allowNull: false,
            defaultValue: false,
            type: Sequelize.BOOLEAN
          }
        }, { transaction })
      ))
      .then(() => (queryInterface.createTable('user_auth_tokens', {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          allowNull: false,
          primaryKey: true
        },
        user_auth_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'user_auths',
            key: 'id'
          }
        },
        token: {
          type: Sequelize.STRING,
          allowNull: false
        },
        expiry_date: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        created_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: false,
          defaultValue: Sequelize.literal('NOW()'),
          type: Sequelize.DATE
        },
        disabled: {
          allowNull: false,
          defaultValue: false,
          type: Sequelize.BOOLEAN
        }
      }, { transaction })
      ))
      .then(() => getUsers(queryInterface, transaction))
      .then(users => {
        return Promise.all(users.map(user => makeUserAuthFromUser(queryInterface, transaction, user)))
      })
  )),
  down: (queryInterface: QueryInterface) => (
    queryInterface.sequelize.transaction(transaction => (
      Promise.resolve()
        .then(() => queryInterface.dropTable('user_auth_tokens', { transaction }))
        .then(() => queryInterface.dropTable('user_auth_resets', { transaction }))
        .then(() => queryInterface.dropTable('user_auths', { transaction }))
    ))
  )
}