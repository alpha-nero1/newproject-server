#/bin/bash
# Ssh into EC2.
ssh -T ec2-user@ec2-3-16-14-122.us-east-2.compute.amazonaws.com << EOF
printf "\n** INFO: Starting ssh agent and authenticating."
ssh-agent bash;
ssh-add /home/ec2-user/id_rsa;

# Git-crypt unlock
printf "\n** INFO: Unlocking configz"
cd newproject && git pull

# Get into server folder.
printf "\n** INFO: Getting into right folder for npm install: "
cd server && pwd

# Run install.
printf "\n** INFO: Running npm install."
npm --version
npm i --verbose

# Build the typescript.
printf "\n** INFO: Running tsc from installed typescript package."
./node_modules/typescript/bin/tsc

cd built

# Run the app.
node app.js;
# App is running (port 8000 specified in app.js):
printf "\n** INFO: App running at: https://ec2-18-225-32-223.us-east-2.compute.amazonaws.com:8000"
EOF